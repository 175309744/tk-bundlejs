import Vue from 'vue'
import weex from 'weex-vue-render'
let Embed = require('@/components/embed.vue')
weex.registerModule('utils', {
  openURL (url) {
    window.location.href = url
  }
})

weex.init(Vue)
// weex.registerComponent('sidebar', Sidebar)
// 或者使用 Vue.component
Vue.component('embed', Embed)
