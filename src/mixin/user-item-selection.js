let itemSelectionCart = require('../include/item-selection-cart')
module.exports = {
  data: function () {
    return {
      itemSelectionCart: itemSelectionCart
    }
  },
  components: {
    'user-item-selection': require('../components/user-item-selection.vue')
  },
  methods: {
    initUserItemSelection () {
      return itemSelectionCart.initUserItemSelection().done(rs => {

      })
    },
    selectItemToSelection (item, flag) {
      return itemSelectionCart.selectItemToSelection(item, flag)
    },
    checkItemSelected (item) {
      return itemSelectionCart.checkItemSelected(item)
    }
  }
}
