let Deferred = require('Deferred')
module.exports = {
  data: function () {
    return {
      refreshing: false,
      indicatorScale: 0.2,
      refreshPullingDown: false
    }
  },
  components: {
    'page-refresh': require('../components/page-refresh.vue')
  },
  methods: {
    onRefresh (event, callback) {
      this.refreshing = true
      Deferred.when(callback).always(() => {
        this.refreshPullingDown = false
        // this.indicatorScale = .2
        setTimeout(() => {
          this.refreshing = false
        }, 300)
      })
    },
    onPullingDown (event) {
      if (this.refreshing) {
        return
      }
      this.refreshPullingDown = true
      let scale = Math.abs(event.pullingDistance) / (event.viewHeight / 2) * 40 / 100
      this.indicatorScale = scale > 1 ? 1 : scale
    }
  }
}
