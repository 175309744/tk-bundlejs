module.exports = {
  components: {
    'page-scrolltop': require('../components/page-scrolltop.vue')
  },
  data () {
    return {
      pageScrollTopShown: false
    }
  },
  methods: {
    onPageScrollForScrollTop (event) {
      let y = Math.abs(event.contentOffset.y)
      if (y > this.$getConfig().env.deviceHeight * 2 / 3) {
        this.pageScrollTopShown = true
      } else {
        this.pageScrollTopShown = false
      }
    }
  }
}
