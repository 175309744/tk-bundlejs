let navigator = require('./xnavigator')
let queryParser = require('./queryparser')
let api = require('./api')
module.exports = function (pageJump) {
  if (!pageJump.page) {
    return
  }
  var page = ''
  if (pageJump.page === 'COMMISSION_ITEM') {
    page = 'commission-item.js'
  } else if (pageJump.page === 'COMMISSION_ITEM_SEARCH') {
    page = 'commission-item-search.js'
  } else if (pageJump.page === 'COMMISSION_ITEM_EP_SEARCH') {
    page = 'commission-item-search-slider.js'
  } else if (pageJump.page === 'WEB_PAGE') {
    page = 'webpage.js'
  } else if (pageJump.page === 'FREE_CHARGE_ACTIVITY') {
    page = 'free-charge-activity.js'
  } else if (pageJump.page === 'FLASH_SALE') {
    page = 'flash-sale.js'
  } else if (pageJump.page === 'URL') {
    let wxUtils = weex.requireModule('utils')
    let params = queryParser(pageJump.params)
    api.placeholder.clean({
      text: params.url
    }).done(rs => {
      wxUtils.openURL(rs.data)
    })
    return
  } else if (pageJump.page === 'PAGE') {
    let params = queryParser(pageJump.params)
    navigator.push(params.page)
    return
  } else {
    return
  }
  let params = pageJump.params || ''
  navigator.push({
    url: page + '?' + params
  })
}
