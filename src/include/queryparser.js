module.exports = (href) => {
  var queryString = ''
  var param = {}
  if (href.indexOf('#') !== -1) {
    queryString = href.substring(href.indexOf('?') + 1, href.indexOf('#'))
  } else {
    queryString = href.substring(href.indexOf('?') + 1)
  }
  if (queryString !== '') {
    var ary = queryString.split('&')
    for (var i = 0; i < ary.length; i++) {
      var kv = ary[i]
      var tmps = kv.split('=')
      if (tmps.length > 1) {
        let key = tmps[0]
        let val = decodeURIComponent(tmps[1])
        if (param[tmps[0]]) {
          if (typeof param[key] === typeof 'array') {
            param[key].push(val)
          } else {
            param[key] = [val]
          }
        } else {
          param[key] = val
        }
      }
    }
  }
  return param
}
