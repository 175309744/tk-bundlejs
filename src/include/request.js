var extend = require('./extend')
var urlPath = require('./url-path')
var stream = weex.requireModule('stream')
var Deferred = require('Deferred')
var serialize = require('./serialize')
var login = require('./login')
var loginDoing = false
var modal = weex.requireModule('modal')
var loginOptions = []
let broadcastChannelKeys = require('./broadcast-channel-keys')
let BroadcastChannel = require('./broadcast-channel')
let loginBroadcastChannel = new BroadcastChannel(broadcastChannelKeys.LOGIN)
let navigator = require('./xnavigator')
loginBroadcastChannel.onmessage = (e) => {
  loginDoing = false
}
var API = {
  request: function (option) {
    option = extend({}, option || {})
    option.headers = option.headers || {}
    if (!option.headers['Content-Type']) {
      option.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
    }
    let deferred = new Deferred()
    deferred.fail(function (data, res) {
      if (res.errMsg) {
        modal.alert({
          title: '信息提示',
          message: res.errMsg.split(' ')[1],
          success: function () {
          }
        })
      }
    })
    stream.fetch(option, function (res) {
      var data = res.data
      if (res === undefined || data === undefined) {
        deferred.reject(data, res)
        return
      }
      if (data.success) {
        deferred.resolve(res.data, res)
      } else {
        if (data.code === 403) {
          if (loginDoing) {
            loginOptions.push(option)
            return
          }
          loginOptions.push(option)
          loginDoing = true
          login.invalidate().done(() => {
            login.login(() => {
              loginDoing = false
              while (loginOptions.length) {
                API.request(loginOptions.pop()).done(function () {
                  deferred.resolve.apply(deferred, Array.prototype.slice.call(arguments))
                }).fail(rs => {
                  deferred.reject.apply(deferred, Array.prototype.slice.call(arguments))
                })
              }
            })
          })
        } else if (data.code === 601) {
          if (data.data === 'SUPER') {
            modal.confirm({
              message: '对不起，您还不是超级会员，无法进行该操作！',
              duration: 0.3,
              okTitle: '升级超级会员',
              cancelTitle: '关闭'
            }, function (value) {
              if (value === '升级超级会员') {
                navigator.push('upgrade-user-level.js')
              }
            })
          }
        } else {
          if (data.level === 'SYSTEM') {
            modal.alert({
              title: '信息提示',
              message: data.errMsg,
              success: function (res) {
              }
            })
            deferred.reject(res.data, res)
          }
        }
      }
    }, function () {
      deferred.notify.apply(deferred, Array.prototype.slice.call(arguments))
    })
    return deferred
  },
  do_request: function (url, param, method, type) {
    if (typeof url === 'function') {
      var tmp = url(param)
      if (typeof tmp === 'string') {
        url = tmp
      } else {
        url = tmp[0]
        if (tmp.length > 1) {
          param = tmp[1]
        }
      }
    }
    url = urlPath.api(url)
    // modal.alert({message: url})
    // 处理url中的placeholder
    url = url.replace(/\{([^}]+)}/gi, function ($0, $1) {
      var val = param[$1]
      delete param[$1]
      return val
    })
    if (method.toLowerCase() === 'get') {
      if (url.indexOf('?') === -1) {
        url += '?'
      }
      url = url + (url.indexOf('&') === -1 ? '' : '&') + (typeof param !== 'string' ? serialize(param) : (param === undefined ? '' : param))
      param = ''
    }
    // modal.alert({message: JSON.stringify(param)})
    return this.request({
      url: url,
      type: type,
      body: typeof param !== 'string' ? serialize(param) : (param === undefined ? '' : param),
      method: method
    })
  },
  createFunction: function (url, method) {
    var self = this
    return function (body, type, listener) {
      listener && listener.onRequest && listener.onRequest()
      return self.do_request(url, body, method || 'GET', type || 'json').done(rs => {
        listener && listener.onSuccess && listener.onSuccess(rs)
      }).fail((rs) => {
        listener && listener.onFail && listener.onFail(rs)
      }).always(rs => {
        listener && listener.onFinish && listener.onFinish(rs)
      })
    }
  },
  createUrl: function (url) {
    url = urlPath.api(url)
    return function (param) {
      return url.replace(/\{([^}]+)}/gi, function ($0, $1) {
        var val = (typeof param === 'string') ? param : param[$1]
        return val === undefined ? '' : val
      })
    }
  }
}

module.exports = API
