let bundleJsService = weex.requireModule('bundleJsService')
let bundleJsServer = 'https://bundlejs.mizugou.com'
let storageKeys = require('./storage-keys')
let storage = require('./xstorage')
let platform = require('./platform')
if (typeof weex.config.env.debug === 'string') {
  weex.config.env.debug = weex.config.env.debug === 'true'
}

var DEBUG = weex.config.env.debug

var mode = 'my'
var assetsBase, pageBase, apiBase
function setEnv (env) {
  switch (env) {
    case 'pro':
      assetsBase = 'https://imgs.mizugou.com/assets'
      pageBase = 'https://bundlejs.mizugou.com/'
      bundleJsServer = 'https://bundlejs.mizugou.com'
      apiBase = 'https://apix.mizugou.com'
      break
    case 'my':
      assetsBase = 'http://192.168.31.174/mz-assets'
      if (platform.isWeb()) {
        pageBase = 'http://192.168.31.174:8081/'
      } else {
        pageBase = 'http://192.168.31.174/mz/'
      }
      apiBase = 'http://192.168.31.174:8083'
      DEBUG = true
      break
    case 'my1':
      assetsBase = 'http://192.168.1.26/mz-assets'
      if (platform.isWeb()) {
        pageBase = 'http://192.168.1.26:8081/'
      } else {
        pageBase = 'http://192.168.1.26/mz/'
      }
      apiBase = 'http://192.168.1.26:8083'
      DEBUG = true
      break
    case 'other':
      assetsBase = 'http://192.168.25.120/mz-assets'
      if (platform.isWeb()) {
        pageBase = 'http://192.168.25.120:8081/'
      } else {
        pageBase = 'http://192.168.25.120/mz/'
      }
      apiBase = 'http://192.168.25.120:8083'
      DEBUG = true
      break
    case 'staging':
      assetsBase = 'https://imgs.mizugou.com/assets'
      pageBase = 'http://bundlejs-mlg.oss-cn-beijing.aliyuncs.com/staging/'
      bundleJsServer = 'http://bundlejs-mlg.oss-cn-beijing.aliyuncs.com/staging/'
      apiBase = 'https://apix.mizugou.com'
      DEBUG = true
      break
  }
}
setEnv(mode)

// config.config().done(cfg => {
//   if((cfg.debug && cfg.devEnv) || weex.config.env.debug) {
//     mode = 'dev'
//     setEnv(mode)
//   }
// })

function wrapVersion (path, flag) {
  if (!flag) {
    return path
  }
  if (path.indexOf('?') !== -1) {
    path += '&_=' + new Date().getTime()
  } else {
    path += '?_=' + new Date().getTime()
  }
  return path
}

if (!pageBase.endsWith('/')) {
  pageBase += '/'
}
module.exports = {
  assets (path, version) {
    let base = assetsBase
    return wrapVersion(base + '/' + path, version)
  },
  page (path, version, callback) {
    if (platform.isWeb()) {
      path = path.replace('.js', '.html')
    }
    if (typeof version === 'function') {
      callback = version
      version = null
    }
    let base = pageBase
    if (path.startsWith('//')) {
      base = 'http:'
    } else if (path.startsWith('http://') || path.startsWith('https://')) {
      base = ''
    } else if (!DEBUG) {
      if (platform.isWeb()) {
        callback && callback(wrapVersion(pageBase + path, version), path)
        return
      }
      storage.getItem(storageKeys.BUNDLE_JS_VERSION).done(version => {
        let server = DEBUG ? pageBase : bundleJsServer + '/' + version + '/'
        let pathSeg = path.split('?')
        let basePath = pathSeg[0]
        bundleJsService.local({
          remote: server + basePath + '?_=' + new Date().getTime(),
          local: basePath
        }, localJs => {
          callback && callback(wrapVersion(localJs + (pathSeg.length > 1 ? '?' + pathSeg[1] : ''), version))
        })
      })
      return
    }
    callback && callback(wrapVersion(base + path, version), path)
  },
  api (path, version) {
    let base = apiBase
    if (!path.startsWith('/')) {
      path = '/' + path
    }
    return wrapVersion(base + path, version)
  }
}
