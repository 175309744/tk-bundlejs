module.exports = {
  isWeb () {
    return weex.config.env.platform.toUpperCase() === 'WEB'
  },
  isIOS () {
    return weex.config.env.platform.toUpperCase() === 'IOS'
  }
  ,
  isAndroid () {
    return weex.config.env.platform.toUpperCase() === 'ANDROID'
  }
}
