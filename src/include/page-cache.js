let storage = require('./xstorage')
module.exports = {
  cache (page, data) {
    let KEY = 'PAGE_DATE_CACHE_' + page
    if (arguments.length === 1) {
      return storage.getItem(KEY)
    } else {
      return storage.setItem(KEY, data)
    }
  }
}
