let storageKeys = require('../include/storage-keys')
let storage = require('../include/xstorage')
let Deferred = require('Deferred')
let platform = require('../include/platform')
module.exports = {
  config (appConfig, value) {
    if (arguments.length === 0) {
      return storage.getItem(storageKeys.CONFIG, {})
    }
    var deferred = new Deferred()
    if (arguments.length === 1) {
      this.config().done(cfg => {
        Object.assign(cfg, appConfig)
        storage.setItem(storageKeys.CONFIG, cfg).done(_rs => {
          deferred.resolve(_rs)
        }).fail(_rs => {
          deferred.reject(_rs)
        })
      })
      return deferred
    }
    this.config().done(rs => {
      rs[appConfig] = value
      this.config(rs).done(_rs => {
        deferred.resolve(_rs)
      }).fail(_rs => {
        deferred.reject(_rs)
      })
    })
    return deferred
  },
  bindConfig (vm) {
    return this.config().done(rs => {
      rs.iosShow = rs.iosShow || !platform.isIOS()
      Object.assign(vm, rs || {})
    })
  }
}
