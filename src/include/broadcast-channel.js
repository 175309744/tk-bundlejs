function SelfBroadcastChannel () {}

SelfBroadcastChannel.prototype = {
  postMessage () {}
}

try {
  module.exports = BroadcastChannel
} catch (e) {
  module.exports = SelfBroadcastChannel
}
