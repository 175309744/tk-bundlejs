module.exports = function (array, column, processor, fillValue) {
  if (typeof processor !== 'function') {
    fillValue = processor
    processor = null
  }
  var list = []
  var tmpRow = []
  for (var i = 0; i < array.length; i++) {
    var mod = i % column
    if (mod === 0 & i > 0) {
      list.push(tmpRow)
      tmpRow = []
    }
    var val = array[i]
    if (processor) {
      var v = processor(array[i], i)
      if (v !== undefined) {
        val = v
      }
    }
    tmpRow.push(val)
    if (i === array.length - 1 && tmpRow.length > 0) {
      var m = column - tmpRow.length
      if (fillValue) {
        for (var j = 0; j < m; j++) {
          tmpRow.push(fillValue || {})
        }
      }
      list.push(tmpRow)
    }
  }
  return list
}
