let brand = {
  primary: '#ff6600',
  warning: '#e39334',
  success: '#5cb85c',
  danger: '#d9534f'
}
module.exports = {
  tabbar: {
    selectedColor: '#333',
    color: '#333'
  },
  brand: brand,
  color: {
    default: '#474747',
    primary: brand.primary,
    gray: '#555'
  }
}
