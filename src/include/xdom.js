let dom = weex.requireModule('dom')
let Deferred = require('Deferred')
function getComponentRect (ref) {
  var deferred = new Deferred()
  dom.getComponentRect(ref, rs => {
    deferred.resolve(rs)
  })
  return deferred.promise()
}
module.exports = {
  getComponentRect (ref) {
    if (arguments.length > 1) {
      let callbacks = []
      for (let i in arguments) {
        callbacks.push(getComponentRect(arguments[i]))
      }
      return Deferred.when.apply(Deferred, callbacks)
    } else {
      if (ref.constructor === Array) {
        var deferred = new Deferred()
        this.getComponentRect.apply(this, ref).done(function (rs) {
          deferred.resolve(rs)
        })
        return deferred
      } else {
        return getComponentRect(ref)
      }
    }
  }
}
