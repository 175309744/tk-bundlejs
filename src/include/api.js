var client = require('./request')
var login = require('./login')
let Deferred = require('Deferred')
var API_FUNCTION = {
  page: {
    index: {
      data: client.createFunction('/page/index/data', 'GET')
    },
    ucenter: {
      data: login.decorate(client.createFunction('/page/ucenter/data', 'GET'))
    },
    sign: {
      data: login.decorate(client.createFunction('/page/sign/data', 'GET'))
    },
    mission: {
      data: login.decorate(client.createFunction('/page/user-mission/data', 'GET'))
    },
    userInvitation: {
      data: login.decorate(client.createFunction('/page/user-invitation/data', 'GET'))
    },
    commissionItem: {
      data: client.createFunction('/page/commissionItem/data', 'GET')
    },
    commissionItemSearch: {
      data: client.createFunction('/page/commissionItemSearch/data', 'GET')
    },
    my: {
      data: login.decorate(client.createFunction('/page/my/data', 'GET'))
    },
    team: {
      data: login.decorate(client.createFunction('/page/team/data', 'GET'))
    },
    taobaoNewer: {
      data: client.createFunction('/page/taobao-newer/data', 'GET')
    },
    tbPullNewDetailList: {
      data: login.decorate(client.createFunction('/page/tb-pull-new-detail-list/data', 'GET'))
    },
    settings: {
      data: login.decorate(client.createFunction('/page/settings/data', 'GET'))
    },
    applyAgent: {
      data: client.createFunction('/page/apply-agent/data', 'GET')
    },
    search: {
      data: client.createFunction('/page/search/data', 'GET')
    },
    myBalance: {
      data: login.decorate(client.createFunction('/page/my-balance/data', 'GET'))
    },
    upgradeUserLevel: {
      data: client.createFunction('/page/upgrade-user-level/data', 'GET')
    },
    inviteUser: {
      data: login.decorate(client.createFunction('/page/invite-user/data', 'GET'))
    },
    orderPay: {
      data: login.decorate(client.createFunction('/page/order-pay/data/{id}', 'GET'))
    },
    sellerPromotionItem: {
      data: client.createFunction('/page/seller-promotion-item/data', 'GET')
    },
    flashSale: {
      data: client.createFunction('/page/flash-sale/data', 'GET')
    },
    earning: {
      data: login.decorate(client.createFunction('/page/earning/data', 'GET'))
    },
    myAgent: {
      data: login.decorate(client.createFunction('/page/my-agent/data', 'GET'))
    },
    customerService: {
      data: client.createFunction('/page/customer-service/data', 'GET')
    },
    userWithdraw: {
      data: client.createFunction('/page/user-withdraw/data/{id}', 'GET')
    },
    freeChargeActivity: {
      data: client.createFunction('/page/free-charge-activity/data', 'GET')
    }
  },
  sign: login.decorate(client.createFunction('/sign', 'POST')),
  shopcat: {
    list: client.createFunction('/shopcat/list/{parentIds}', 'GET'),
    listTree: client.createFunction('/shopcat/list/tree', 'GET')
  },
  search: {
    commissionItem: client.createFunction('/search/commission-item', 'GET'),
    commissionItemSameStyle: client.createFunction('/search/commission-item/same-style/{id}', 'GET')
  },
  commissionItem: {
    info: client.createFunction('/commission-item/{id}', 'GET'),
    shareInfo: login.decorate(client.createFunction('/commission-item/share-info/{id}', 'GET')),
    qg: client.createFunction('/commission-item/qg/{id}', 'GET'),
    goToBuyParam: login.decorate(client.createFunction('/commission-item/go-to-buy/{id}', 'GET'))
  },
  item: {
    descImgList: client.createFunction('/item/desc-img-list/{id}', 'GET')
  },
  team: {
    memberList: login.decorate(client.createFunction('/team/member-list', 'GET'))
  },
  userCommissionOrder: {
    search: login.decorate(client.createFunction('/user-commission-order/search', 'GET'))
  },
  tbk: {
    tpwd: client.createFunction('/tbk/tpwd/{itemId}', 'GET'),
    activityLink: login.decorate(client.createFunction('/tbk/activitylink', 'POST'))
  },
  url: {
    commissionItem: {
      sharePic: client.createUrl('/commission-item/share-pic/{id}-{userId}?picUrl={picUrl}&_={t}'),
      sharePicGen: client.createUrl('/commission-item/share-pic/generate/{id}')
    },
    taobaoNewer: {
      sharePic: client.createUrl('/taobao-newer/share-pic?url={url}&_={t}')
    },
    webpage: client.createUrl('/webpage'),
    invitePic: client.createUrl('/user/invite-pic/{invitationCode}?bgImg={bgImg}')
  },
  login: {
    password: client.createFunction('/login', 'POST'),
    phone: client.createFunction('/login-phone', 'POST'),
    check: client.createFunction('/login-check', 'POST'),
    weixin: client.createFunction('/login/weixin', 'POST'),
    refresh: login.decorate(client.createFunction('/login/refresh', 'GET'))
  },
  findPwd: client.createFunction('/find-pwd', 'POST'),
  logout: client.createFunction('/logout', 'GET'),
  enums: {
    ecomPlat: client.createFunction('/enums/ecomPlat', 'GET')
  },
  security: {
    current: login.decorate(client.createFunction('/security/current', 'GET'))
  },
  sms: {
    register: client.createFunction('/sms/register/{phone}', 'POST'),
    findPwd: client.createFunction('/sms/find-pwd/{phone}', 'POST'),
    phoneLogin: client.createFunction('/sms/phone-login/{phone}', 'POST'),
    phoneBind: client.createFunction('/sms/phone-bind/{phone}', 'POST'),
    withdraw: login.decorate(client.createFunction('/sms/withdraw', 'POST'))
  },
  tbPullNewDetail: {
    search: login.decorate(client.createFunction('/tb-pull-new-detail/search', 'GET'))
  },
  register: {
    register: client.createFunction('/register', 'POST'),
    checkStep1: client.createFunction('/register/check/step1', 'GET')
  },

  boot: client.createFunction('/boot', 'GET'),
  user: {
    bindPhone: login.decorate(client.createFunction('/user/phone-bind', 'POST')),
    updateField: login.decorate(client.createFunction('/user/field', 'POST')),
    bindWeixin: client.createFunction('/user/weixin-bind/{weixinAuthCode}', 'POST'),
    isPlus: login.decorate(client.createFunction('/user/is-plus', 'GET')),
    upgradeUserLevel: login.decorate(client.createFunction('/user/upgrade-user-level', 'PUT')),
    location: client.createFunction('/user/location', 'GET'),
    setDistrict: login.decorate(client.createFunction('/user/district/{id}', 'PUT')),
    modifyPwd: login.decorate(client.createFunction('/user/modify-pwd', 'PUT')),
    checkUserDirectlyUpgrade: login.decorate(client.createFunction('/user/check-user-directly-upgrade/{id}', 'PUT')),
    taobaoRelationId: login.decorate(client.createFunction('/user/taobao-relation-id', 'GET'))
  },
  userAgentApplication: {
    apply: loginRefreshDecorate(login.decorate(client.createFunction('/user-agent-application/apply', 'POST')), rs => {
      return rs.data === true
    })
  },
  walletFlowDetail: {
    search: login.decorate(client.createFunction('/wallet-flow-detail/search', 'GET'))
  },
  userWithdraw: {
    withdraw: login.decorate(client.createFunction('/user-withdraw', 'POST'))
  },
  footmark: {
    search: login.decorate(client.createFunction('/footmark/search', 'GET')),
    delByItemId: login.decorate(client.createFunction('/footmark/item/{id}', 'DELETE')),
    clear: login.decorate(client.createFunction('/footmark/clear', 'DELETE')),
    toggleFavor: login.decorate(client.createFunction('/footmark/toggle-favor/item/{id}', 'POST')),
    favor: login.decorate(client.createFunction('/footmark/favor/item/{id}', 'POST'))
  },
  userItemSelection: {
    create: login.decorate(client.createFunction('/user-item-selection', 'POST')),
    recommendSearch: client.createFunction('/user-item-selection/recommendSearch', 'GET'),
    search: client.createFunction('/user-item-selection/search', 'GET'),
    share: login.decorate(client.createFunction('/user-item-selection/share/{id}', 'GET')),
    del: login.decorate(client.createFunction('/user-item-selection/{id}', 'DELETE'))
  },
  taobaoMaterial: {
    list: client.createFunction('/taobao-material/list', 'GET'),
    catList: client.createFunction('/taobao-material/cat/list', 'GET')
  },
  article: {
    search: client.createFunction('/article/search', 'GET'),
    share: login.decorate(client.createFunction('/article/share/{id}', 'GET'))
  },
  order: {
    appAlipay: login.decorate(client.createFunction('/order/app-alipay/{id}', 'POST'))
  },
  sellerPromotionItem: {
    search: client.createFunction('/seller-promotion-item/search', 'GET'),
    rushBuy: login.decorate(client.createFunction('/seller-promotion-item/rush-buy', 'POST')),
    searchBuy: login.decorate(client.createFunction('/page/seller-promotion-item/search-buy/{id}', 'GET'))
  },
  sellerPromotionItemOrder: {
    fillTradeNo: login.decorate(client.createFunction('/seller-promotion-item-order/fill-trade-no/{id}', 'PUT')),
    search: client.createFunction('/seller-promotion-item-order/search', 'GET')
  },
  flashSaleItem: {
    search: client.createFunction('/flash-sale-item/search', 'GET'),
    draw: client.createFunction('/flash-sale-item/draw/{id}', 'PUT'),
    checkDraw: client.createFunction('/flash-sale-item/check/draw/{id}', 'GET')
  },
  flashSaleOrder: {
    search: client.createFunction('/flash-sale-order/search', 'GET')
  },
  region: {
    level: client.createFunction('/region/level/{level}', 'GET'),
    list: client.createFunction('/region/list/{parentId}', 'GET')
  },
  agentApply: {
    applyCityAgent: login.decorate(client.createFunction('/agent-apply/apply-city-agent', 'POST')),
    applyDistrictAgent: login.decorate(client.createFunction('/agent-apply/apply-district-agent', 'POST'))
  },
  amap: {
    geoCode: {
      regeo: client.createFunction('/amap/geo-code/regeo', 'POST')
    }
  },
  userUpgradeOrder: {
    create: login.decorate(client.createFunction('/user-upgrade-order/{timeUnit}', 'POST'))
  },
  userRedPacket: {
    grantList: client.createFunction('/user-red-packet/grant-list', 'GET'),
    draw: client.createFunction('/user-red-packet/draw/{id}', 'PUT')
  },
  placeholder: {
    clean: client.createFunction('/placeholder/clean', 'POST')
  },
  advSpace: {
    advList: client.createFunction('/adv-space', 'GET')
  },
  freeChargeActivity: {
    checkDraw: login.decorate(client.createFunction('/free-charge-activity/check/draw', 'GET')),
    checkOrder: login.decorate(client.createFunction('/free-charge-activity/check/order', 'POST'))
  },
  freeChargeActivityOrder: {
    search: login.decorate(client.createFunction('/free-charge-activity-order/search', 'GET'))
  },
  homePopup: {
    data: client.createFunction('/home-popup/data', 'GET')
  },
  top: {
    auth: {
      url: login.decorate(client.createFunction('/top/auth/url', 'GET')),
      app: login.decorate(client.createFunction('/top/auth/app', 'POST'))
    }
  },
  flashSaleRemind: {
    join: login.decorate(client.createFunction('/flash-sale-remind/{flashSaleItemId}', 'PUT')),
    exit: login.decorate(client.createFunction('/flash-sale-remind/{flashSaleItemId}', 'DELETE'))
  }
}

function loginRefreshDecorate (fn, predicate) {
  return function () {
    let deferred = new Deferred()
    Deferred.when(fn()).done(function () {
      let args = Array.prototype.slice.call(arguments)
      if (predicate && predicate.apply(null, args)) {
        API_FUNCTION.login.refresh().done(() => {
          deferred.resolve.apply(deferred, args)
        }).fail(() => {
          deferred.reject.apply(deferred, Array.prototype.slice.call(arguments))
        })
      } else {
        deferred.resolve.apply(deferred, args)
      }
    }).fail(function () {
      deferred.reject.apply(deferred, Array.prototype.slice.call(arguments))
    })
    return deferred
  }
}

module.exports = API_FUNCTION
