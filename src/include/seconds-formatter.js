let minSeconds = 60
let hourSeconds = 3600
module.exports = {
  timeInfo (seconds) {
    let hours = parseInt(seconds / hourSeconds)
    seconds -= hours * hourSeconds
    let min = parseInt(seconds / minSeconds)
    seconds -= min * minSeconds
    return {
      val: {
        hour: hours,
        min: min,
        sec: seconds
      },
      hour: hours >= 10 ? hours : '0' + hours,
      min: min >= 10 ? min : '0' + min,
      sec: seconds >= 10 ? seconds : '0' + seconds
    }
  }
}
