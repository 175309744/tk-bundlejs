let Deferred = require('Deferred')
module.exports = () => {
  let deferred = new Deferred()
  let args = []
  var count = 0
  var fail = false
  for (let i = 0; i < arguments.length; i++) {
    ((i, arg) => {
      if (arg.constructor !== Deferred) {
        count++
        args[i] = arg
        return
      }
      arg.done((data) => {
        if (fail) {
          return
        }
        args[i] = data
        count++
        if (count === arguments.length) {
          deferred.resolve.apply(deferred, args)
        }
      }).fail((data) => {
        fail = true
        deferred.reject(i, data)
      })
    })(i, arguments[i])
  }
  return deferred
}
