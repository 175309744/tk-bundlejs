let api = require('./api')
let Deferred = require('Deferred')
let navigator = require('./xnavigator')
let modal = weex.requireModule('modal')
module.exports = {
  checkPlus () {
    let deferred = new Deferred()
    api.user.isPlus().done(rs => {
      if (rs.data) {
        deferred.resolve()
      } else {
        // 在这里打开代理页面
        deferred.reject()
        modal.confirm({
          message: '对不起，您还不是咪乐族人，暂时无法分享，也无法实现自购省钱!',
          duration: 0.3,
          okTitle: '升级咪乐族人',
          cancelTitle: '关闭'
        }, rs => {
          if (rs === '升级咪乐族人') {
            navigator.push('upgrade-user-level.js')
          }
        })
      }
    })
    return deferred
  }
}
