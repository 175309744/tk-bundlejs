let storage = require('./xstorage')
let navigator = require('./xnavigator')
let Deferred = require('Deferred')
let broadcastChannelKeys = require('./broadcast-channel-keys')
let BroadcastChannel = require('./broadcast-channel')
let loginBroadcastChannel = new BroadcastChannel(broadcastChannelKeys.LOGIN)
let storageKeys = require('./storage-keys')
let loginCallbackFnQueue = []
let loginDoing = false
loginBroadcastChannel.onmessage = (e) => {
  loginDoing = false
  if (e.data) {
    for (var i in loginCallbackFnQueue) {
      try {
        loginCallbackFnQueue[i]()
      } catch (e) {

      }
    }
  }
  loginCallbackFnQueue = []
}
module.exports = {
  invalidate () {
    try {
      loginBroadcastChannel.postMessage(false)
    } catch (e) {
      // modal.alert({message: JSON.stringify(e)})
    }
    return storage.removeItem(storageKeys.LOGIN)
  },
  // force 强制登录
  login (fn) {
    loginCallbackFnQueue.push(fn)
    if (loginDoing) {
      return
    }
    loginDoing = true
    navigator.push('login.js')
  },
  check () {
    return storage.getItem(storageKeys.LOGIN)
  },
  success () {
    return storage.setItem(storageKeys.LOGIN, true)
  },
  decorate (fn) {
    let self = this
    return function () {
      let args = Array.prototype.slice.call(arguments)
      let deferred = new Deferred()

      function callFn () {
        Deferred.when(fn.apply(null, args)).done(function () {
          deferred.resolve.apply(deferred, Array.prototype.slice.call(arguments))
        }).fail(function () {
          deferred.reject.apply(deferred, Array.prototype.slice.call(arguments))
        })
      }
      self.check().done(flag => {
        if (flag) {
          callFn()
        } else {
          self.login(callFn)
        }
      })
      return deferred.promise()
    }
  }
}
