let Embed = require('@/components/embed.vue')
weex.registerModule('utils', {
  openURL (url) {
    window.location.href = url
  }
})
weex.registerComponent('embed', Embed)
