// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 662);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function (define) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {

        var forEach = Array.prototype.forEach;
        var hasOwn = Object.prototype.hasOwnProperty;
        var breaker = {};
        var isArray = function isArray(elem) {
            return (typeof elem === "undefined" ? "undefined" : _typeof(elem)) === "object" && elem instanceof Array;
        };
        var isFunction = function isFunction(fn) {
            return typeof fn === "function";
        };

        var // Static reference to slice
        sliceDeferred = [].slice;

        // String to Object flags format cache
        var flagsCache = {};

        // Convert String-formatted flags into Object-formatted ones and store in cache
        function createFlags(flags) {
            var object = flagsCache[flags] = {},
                i,
                length;
            flags = flags.split(/\s+/);
            for (i = 0, length = flags.length; i < length; i++) {
                object[flags[i]] = true;
            }
            return object;
        }

        // Borrowed shamelessly from https://github.com/wookiehangover/underscore.Deferred
        var _each = function _each(obj, iterator, context) {
            var key, i, l;

            if (!obj) {
                return;
            }
            if (forEach && obj.forEach === forEach) {
                obj.forEach(iterator, context);
            } else if (obj.length === +obj.length) {
                for (i = 0, l = obj.length; i < l; i++) {
                    if (i in obj && iterator.call(context, obj[i], i, obj) === breaker) {
                        return;
                    }
                }
            } else {
                for (key in obj) {
                    if (hasOwn.call(obj, key)) {
                        if (iterator.call(context, obj[key], key, obj) === breaker) {
                            return;
                        }
                    }
                }
            }
        };

        var Callbacks = function Callbacks(flags) {

            // Convert flags from String-formatted to Object-formatted
            // (we check in cache first)
            flags = flags ? flagsCache[flags] || createFlags(flags) : {};

            var // Actual callback list
            list = [],

            // Stack of fire calls for repeatable lists
            stack = [],

            // Last fire value (for non-forgettable lists)
            memory,

            // Flag to know if list is currently firing
            firing,

            // First callback to fire (used internally by add and fireWith)
            firingStart,

            // End of the loop when firing
            firingLength,

            // Index of currently firing callback (modified by remove if needed)
            firingIndex,

            // Add one or several callbacks to the list
            _add = function _add(args) {
                var i, length, elem, type, actual;
                for (i = 0, length = args.length; i < length; i++) {
                    elem = args[i];
                    if (isArray(elem)) {
                        // Inspect recursively
                        _add(elem);
                    } else if (isFunction(elem)) {
                        // Add if not in unique mode and callback is not in
                        if (!flags.unique || !self.has(elem)) {
                            list.push(elem);
                        }
                    }
                }
            },

            // Fire callbacks
            fire = function fire(context, args) {
                args = args || [];
                memory = !flags.memory || [context, args];
                firing = true;
                firingIndex = firingStart || 0;
                firingStart = 0;
                firingLength = list.length;
                for (; list && firingIndex < firingLength; firingIndex++) {
                    if (list[firingIndex].apply(context, args) === false && flags.stopOnFalse) {
                        memory = true; // Mark as halted
                        break;
                    }
                }
                firing = false;
                if (list) {
                    if (!flags.once) {
                        if (stack && stack.length) {
                            memory = stack.shift();
                            self.fireWith(memory[0], memory[1]);
                        }
                    } else if (memory === true) {
                        self.disable();
                    } else {
                        list = [];
                    }
                }
            },

            // Actual Callbacks object
            self = {
                // Add a callback or a collection of callbacks to the list
                add: function add() {
                    if (list) {
                        var length = list.length;
                        _add(arguments);
                        // Do we need to add the callbacks to the
                        // current firing batch?
                        if (firing) {
                            firingLength = list.length;
                            // With memory, if we're not firing then
                            // we should call right away, unless previous
                            // firing was halted (stopOnFalse)
                        } else if (memory && memory !== true) {
                            firingStart = length;
                            fire(memory[0], memory[1]);
                        }
                    }
                    return this;
                },
                // Remove a callback from the list
                remove: function remove() {
                    if (list) {
                        var args = arguments,
                            argIndex = 0,
                            argLength = args.length;
                        for (; argIndex < argLength; argIndex++) {
                            for (var i = 0; i < list.length; i++) {
                                if (args[argIndex] === list[i]) {
                                    // Handle firingIndex and firingLength
                                    if (firing) {
                                        if (i <= firingLength) {
                                            firingLength--;
                                            if (i <= firingIndex) {
                                                firingIndex--;
                                            }
                                        }
                                    }
                                    // Remove the element
                                    list.splice(i--, 1);
                                    // If we have some unicity property then
                                    // we only need to do this once
                                    if (flags.unique) {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    return this;
                },
                // Control if a given callback is in the list
                has: function has(fn) {
                    if (list) {
                        var i = 0,
                            length = list.length;
                        for (; i < length; i++) {
                            if (fn === list[i]) {
                                return true;
                            }
                        }
                    }
                    return false;
                },
                // Remove all callbacks from the list
                empty: function empty() {
                    list = [];
                    return this;
                },
                // Have the list do nothing anymore
                disable: function disable() {
                    list = stack = memory = undefined;
                    return this;
                },
                // Is it disabled?
                disabled: function disabled() {
                    return !list;
                },
                // Lock the list in its current state
                lock: function lock() {
                    stack = undefined;
                    if (!memory || memory === true) {
                        self.disable();
                    }
                    return this;
                },
                // Is it locked?
                locked: function locked() {
                    return !stack;
                },
                // Call all callbacks with the given context and arguments
                fireWith: function fireWith(context, args) {
                    if (stack) {
                        if (firing) {
                            if (!flags.once) {
                                stack.push([context, args]);
                            }
                        } else if (!(flags.once && memory)) {
                            fire(context, args);
                        }
                    }
                    return this;
                },
                // Call all the callbacks with the given arguments
                fire: function fire() {
                    self.fireWith(this, arguments);
                    return this;
                },
                // To know if the callbacks have already been called at least once
                fired: function fired() {
                    return !!memory;
                }
            };

            return self;
        };

        var Deferred = function Deferred(func) {
            var doneList = Callbacks("once memory"),
                failList = Callbacks("once memory"),
                progressList = Callbacks("memory"),
                _state = "pending",
                lists = {
                resolve: doneList,
                reject: failList,
                notify: progressList
            },
                _promise = {
                done: doneList.add,
                fail: failList.add,
                progress: progressList.add,

                state: function state() {
                    return _state;
                },

                // Deprecated
                isResolved: doneList.fired,
                isRejected: failList.fired,

                then: function then(doneCallbacks, failCallbacks, progressCallbacks) {
                    deferred.done(doneCallbacks).fail(failCallbacks).progress(progressCallbacks);
                    return this;
                },
                always: function always() {
                    deferred.done.apply(deferred, arguments).fail.apply(deferred, arguments);
                    return this;
                },
                pipe: function pipe(fnDone, fnFail, fnProgress) {
                    return Deferred(function (newDefer) {
                        _each({
                            done: [fnDone, "resolve"],
                            fail: [fnFail, "reject"],
                            progress: [fnProgress, "notify"]
                        }, function (data, handler) {
                            var fn = data[0],
                                action = data[1],
                                returned;
                            if (isFunction(fn)) {
                                deferred[handler](function () {
                                    returned = fn.apply(this, arguments);
                                    if (returned && isFunction(returned.promise)) {
                                        returned.promise().then(newDefer.resolve, newDefer.reject, newDefer.notify);
                                    } else {
                                        newDefer[action + "With"](this === deferred ? newDefer : this, [returned]);
                                    }
                                });
                            } else {
                                deferred[handler](newDefer[action]);
                            }
                        });
                    }).promise();
                },
                // Get a promise for this deferred
                // If obj is provided, the promise aspect is added to the object
                promise: function promise(obj) {
                    if (!obj) {
                        obj = _promise;
                    } else {
                        for (var key in _promise) {
                            obj[key] = _promise[key];
                        }
                    }
                    return obj;
                }
            },
                deferred = _promise.promise({}),
                key;

            for (key in lists) {
                deferred[key] = lists[key].fire;
                deferred[key + "With"] = lists[key].fireWith;
            }

            // Handle state
            deferred.done(function () {
                _state = "resolved";
            }, failList.disable, progressList.lock).fail(function () {
                _state = "rejected";
            }, doneList.disable, progressList.lock);

            // Call given func if any
            if (func) {
                func.call(deferred, deferred);
            }

            // All done!
            return deferred;
        };

        // Deferred helper
        var when = function when(firstParam) {
            var args = sliceDeferred.call(arguments, 0),
                i = 0,
                length = args.length,
                pValues = new Array(length),
                count = length,
                pCount = length,
                deferred = length <= 1 && firstParam && isFunction(firstParam.promise) ? firstParam : Deferred(),
                promise = deferred.promise();
            function resolveFunc(i) {
                return function (value) {
                    args[i] = arguments.length > 1 ? sliceDeferred.call(arguments, 0) : value;
                    if (! --count) {
                        deferred.resolveWith(deferred, args);
                    }
                };
            }
            function progressFunc(i) {
                return function (value) {
                    pValues[i] = arguments.length > 1 ? sliceDeferred.call(arguments, 0) : value;
                    deferred.notifyWith(promise, pValues);
                };
            }
            if (length > 1) {
                for (; i < length; i++) {
                    if (args[i] && args[i].promise && isFunction(args[i].promise)) {
                        args[i].promise().then(resolveFunc(i), deferred.reject, progressFunc(i));
                    } else {
                        --count;
                    }
                }
                if (!count) {
                    deferred.resolveWith(deferred, args);
                }
            } else if (deferred !== firstParam) {
                deferred.resolveWith(deferred, length ? [firstParam] : []);
            }
            return promise;
        };

        Deferred.when = when;
        Deferred.Callbacks = Callbacks;

        return Deferred;
    }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)); // define for AMD if available
})(__webpack_require__(23));

/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
  LOGIN: "LOGIN",
  REGISTER: "REGISTER",
  REGISTER_OK: 'REGISTER_OK',
  WEIXIN_AUTH: 'WEIXIN_AUTH',
  BASE_VIEW: 'BASE_VIEW',
  ITEM_SELECTION_CART: 'ITEM_SELECTION_CART',
  LOGIN_RESULT: 'LOGIN_RESULT',
  PHONE_LOGIN: 'PHONE_LOGIN'
};

/***/ }),

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var storage = __webpack_require__(7);
var navigator = __webpack_require__(4);
var Deferred = __webpack_require__(1);
var broadcastChannelKeys = __webpack_require__(12);
var loginBroadcastChannel = new BroadcastChannel(broadcastChannelKeys.LOGIN);
var storageKeys = __webpack_require__(8);
var loginCallbackFnQueue = [];
var loginDoing = false;
loginBroadcastChannel.onmessage = function (e) {
  loginDoing = false;
  if (e.data) {
    for (var i in loginCallbackFnQueue) {
      try {
        loginCallbackFnQueue[i]();
      } catch (e) {}
    }
  }
  loginCallbackFnQueue = [];
};
module.exports = {
  invalidate: function invalidate() {
    try {
      loginBroadcastChannel.postMessage(false);
    } catch (e) {
      // modal.alert({message: JSON.stringify(e)})
    }
    return storage.removeItem(storageKeys.LOGIN);
  },

  // force 强制登录
  login: function login(fn) {
    loginCallbackFnQueue.push(fn);
    if (loginDoing) {
      return;
    }
    loginDoing = true;
    navigator.push('login.js');
  },
  check: function check() {
    return storage.getItem(storageKeys.LOGIN);
  },
  success: function success() {
    return storage.setItem(storageKeys.LOGIN, true);
  },
  decorate: function decorate(fn) {
    var self = this;
    return function () {
      var args = Array.prototype.slice.call(arguments);
      var deferred = new Deferred();

      function callFn() {
        Deferred.when(fn.apply(null, args)).done(function () {
          deferred.resolve.apply(deferred, Array.prototype.slice.call(arguments));
        }).fail(function () {
          deferred.reject.apply(deferred, Array.prototype.slice.call(arguments));
        });
      }
      self.check().done(function (flag) {
        if (flag) {
          callFn();
        } else {
          self.login(callFn);
        }
      });
      return deferred.promise();
    };
  }
};

/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var client = __webpack_require__(24);
var login = __webpack_require__(13);
var Deferred = __webpack_require__(1);
var API_FUNCTION = {
  page: {
    index: {
      data: client.createFunction('/page/index/data', 'GET')
    },
    ucenter: {
      data: login.decorate(client.createFunction('/page/ucenter/data', 'GET'))
    },
    sign: {
      data: login.decorate(client.createFunction('/page/sign/data', 'GET'))
    },
    mission: {
      data: login.decorate(client.createFunction('/page/user-mission/data', 'GET'))
    },
    userInvitation: {
      data: login.decorate(client.createFunction('/page/user-invitation/data', 'GET'))
    },
    commissionItem: {
      data: client.createFunction('/page/commissionItem/data', 'GET')
    },
    commissionItemSearch: {
      data: client.createFunction('/page/commissionItemSearch/data', 'GET')
    },
    my: {
      data: login.decorate(client.createFunction('/page/my/data', 'GET'))
    },
    team: {
      data: login.decorate(client.createFunction('/page/team/data', 'GET'))
    },
    taobaoNewer: {
      data: client.createFunction('/page/taobao-newer/data', 'GET')
    },
    tbPullNewDetailList: {
      data: login.decorate(client.createFunction('/page/tb-pull-new-detail-list/data', 'GET'))
    },
    settings: {
      data: login.decorate(client.createFunction('/page/settings/data', 'GET'))
    },
    applyAgent: {
      data: client.createFunction('/page/apply-agent/data', 'GET')
    },
    search: {
      data: client.createFunction('/page/search/data', 'GET')
    },
    myBalance: {
      data: login.decorate(client.createFunction('/page/my-balance/data', 'GET'))
    },
    upgradeUserLevel: {
      data: client.createFunction('/page/upgrade-user-level/data', 'GET')
    },
    inviteUser: {
      data: login.decorate(client.createFunction('/page/invite-user/data', 'GET'))
    },
    orderPay: {
      data: login.decorate(client.createFunction('/page/order-pay/data/{id}', 'GET'))
    },
    sellerPromotionItem: {
      data: client.createFunction('/page/seller-promotion-item/data', 'GET')
    },
    flashSale: {
      data: client.createFunction('/page/flash-sale/data', 'GET')
    },
    earning: {
      data: login.decorate(client.createFunction('/page/earning/data', 'GET'))
    },
    myAgent: {
      data: login.decorate(client.createFunction('/page/my-agent/data', 'GET'))
    },
    customerService: {
      data: client.createFunction('/page/customer-service/data', 'GET')
    },
    userWithdraw: {
      data: client.createFunction('/page/user-withdraw/data/{id}', 'GET')
    },
    freeChargeActivity: {
      data: client.createFunction('/page/free-charge-activity/data', 'GET')
    }
  },
  sign: login.decorate(client.createFunction('/sign', 'POST')),
  shopcat: {
    list: client.createFunction('/shopcat/list/{parentIds}', 'GET'),
    listTree: client.createFunction('/shopcat/list/tree', 'GET')
  },
  search: {
    commissionItem: client.createFunction('/search/commission-item', 'GET'),
    commissionItemSameStyle: client.createFunction('/search/commission-item/same-style/{id}', 'GET')
  },
  commissionItem: {
    info: client.createFunction('/commission-item/{id}', 'GET'),
    shareInfo: login.decorate(client.createFunction('/commission-item/share-info/{id}', 'GET')),
    qg: client.createFunction('/commission-item/qg/{id}', 'GET'),
    goToBuyParam: login.decorate(client.createFunction('/commission-item/go-to-buy/{id}', 'GET'))
  },
  item: {
    descImgList: client.createFunction('/item/desc-img-list/{id}', 'GET')
  },
  team: {
    memberList: login.decorate(client.createFunction('/team/member-list', 'GET'))
  },
  userCommissionOrder: {
    search: login.decorate(client.createFunction('/user-commission-order/search', 'GET'))
  },
  tbk: {
    tpwd: client.createFunction('/tbk/tpwd/{itemId}', 'GET')
  },
  url: {
    commissionItem: {
      sharePic: client.createUrl('/commission-item/share-pic/{id}-{userId}?picUrl={picUrl}&_={t}'),
      sharePicGen: client.createUrl('/commission-item/share-pic/generate/{id}')
    },
    taobaoNewer: {
      sharePic: client.createUrl('/taobao-newer/share-pic?url={url}&_={t}')
    },
    webpage: client.createUrl('/webpage'),
    invitePic: client.createUrl('/user/invite-pic/{invitationCode}?bgImg={bgImg}')
  },
  login: {
    password: client.createFunction('/login', 'POST'),
    phone: client.createFunction('/login-phone', 'POST'),
    check: client.createFunction('/login-check', 'POST'),
    weixin: client.createFunction('/login/weixin', 'POST'),
    refresh: login.decorate(client.createFunction('/login/refresh', 'GET'))

  },
  findPwd: client.createFunction('/find-pwd', 'POST'),
  logout: client.createFunction('/logout', 'GET'),
  enums: {
    ecomPlat: client.createFunction('/enums/ecomPlat', 'GET')
  },
  security: {
    current: login.decorate(client.createFunction('/security/current', 'GET'))
  },
  sms: {
    register: client.createFunction('/sms/register/{phone}', 'POST'),
    findPwd: client.createFunction('/sms/find-pwd/{phone}', 'POST'),
    phoneLogin: client.createFunction('/sms/phone-login/{phone}', 'POST'),
    phoneBind: client.createFunction('/sms/phone-bind/{phone}', 'POST'),
    withdraw: login.decorate(client.createFunction('/sms/withdraw', 'POST'))
  },
  tbPullNewDetail: {
    search: login.decorate(client.createFunction('/tb-pull-new-detail/search', 'GET'))
  },
  register: {
    register: client.createFunction('/register', 'POST'),
    checkStep1: client.createFunction('/register/check/step1', 'GET')
  },

  boot: client.createFunction('/boot', 'GET'),
  user: {
    bindPhone: login.decorate(client.createFunction('/user/phone-bind', 'POST')),
    updateField: login.decorate(client.createFunction('/user/field', 'POST')),
    bindWeixin: client.createFunction('/user/weixin-bind/{weixinAuthCode}', 'POST'),
    isPlus: login.decorate(client.createFunction('/user/is-plus', 'GET')),
    upgradeUserLevel: login.decorate(client.createFunction('/user/upgrade-user-level', 'PUT')),
    location: client.createFunction('/user/location', 'GET'),
    setDistrict: login.decorate(client.createFunction('/user/district/{id}', 'PUT')),
    modifyPwd: login.decorate(client.createFunction('/user/modify-pwd', 'PUT')),
    checkUserDirectlyUpgrade: login.decorate(client.createFunction('/user/check-user-directly-upgrade/{id}', 'PUT'))
  },
  userAgentApplication: {
    apply: login_refresh_decorate(login.decorate(client.createFunction('/user-agent-application/apply', 'POST')), function (rs) {
      return rs.data === true;
    })
  },
  walletFlowDetail: {
    search: login.decorate(client.createFunction('/wallet-flow-detail/search', 'GET'))
  },
  userWithdraw: {
    withdraw: login.decorate(client.createFunction('/user-withdraw', 'POST'))
  },
  footmark: {
    search: login.decorate(client.createFunction('/footmark/search', 'GET')),
    delByItemId: login.decorate(client.createFunction('/footmark/item/{id}', 'DELETE')),
    clear: login.decorate(client.createFunction('/footmark/clear', 'DELETE')),
    toggleFavor: login.decorate(client.createFunction('/footmark/toggle-favor/item/{id}', 'POST')),
    favor: login.decorate(client.createFunction('/footmark/favor/item/{id}', 'POST'))
  },
  userItemSelection: {
    create: login.decorate(client.createFunction('/user-item-selection', 'POST')),
    recommendSearch: client.createFunction('/user-item-selection/recommendSearch', 'GET'),
    search: client.createFunction('/user-item-selection/search', 'GET'),
    share: login.decorate(client.createFunction('/user-item-selection/share/{id}', 'GET')),
    del: login.decorate(client.createFunction('/user-item-selection/{id}', 'DELETE'))
  },
  article: {
    search: client.createFunction('/article/search', 'GET'),
    share: login.decorate(client.createFunction('/article/share/{id}', 'GET'))
  },
  order: {
    appAlipay: login.decorate(client.createFunction('/order/app-alipay/{id}', 'POST'))
  },
  sellerPromotionItem: {
    search: client.createFunction('/seller-promotion-item/search', 'GET'),
    rushBuy: login.decorate(client.createFunction('/seller-promotion-item/rush-buy', 'POST')),
    searchBuy: login.decorate(client.createFunction('/page/seller-promotion-item/search-buy/{id}', 'GET'))
  },
  sellerPromotionItemOrder: {
    fillTradeNo: login.decorate(client.createFunction('/seller-promotion-item-order/fill-trade-no/{id}', 'PUT')),
    search: client.createFunction('/seller-promotion-item-order/search', 'GET')
  },
  flashSaleItem: {
    search: client.createFunction('/flash-sale-item/search', 'GET')
  },
  region: {
    level: client.createFunction('/region/level/{level}', 'GET'),
    list: client.createFunction('/region/list/{parentId}', 'GET')
  },
  agentApply: {
    applyCityAgent: login.decorate(client.createFunction('/agent-apply/apply-city-agent', 'POST')),
    applyDistrictAgent: login.decorate(client.createFunction('/agent-apply/apply-district-agent', 'POST'))
  },
  amap: {
    geoCode: {
      regeo: client.createFunction('/amap/geo-code/regeo', 'POST')
    }
  },
  userUpgradeOrder: {
    create: login.decorate(client.createFunction('/user-upgrade-order/{timeUnit}', 'POST'))
  },
  userRedPacket: {
    grantList: client.createFunction('/user-red-packet/grant-list', 'GET'),
    draw: client.createFunction('/user-red-packet/draw/{id}', 'PUT')
  },
  placeholder: {
    clean: client.createFunction('/placeholder/clean', 'POST')
  },
  advSpace: {
    advList: client.createFunction('/adv-space', 'GET')
  },
  freeChargeActivity: {
    checkDraw: login.decorate(client.createFunction('/free-charge-activity/check/draw', 'GET')),
    checkOrder: login.decorate(client.createFunction('/free-charge-activity/check/order', 'POST'))
  },
  freeChargeActivityOrder: {
    search: login.decorate(client.createFunction('/free-charge-activity-order/search', 'GET'))
  },
  homePopup: {
    data: client.createFunction('/home-popup/data', 'GET')
  }
};

function login_refresh_decorate(fn, predicate) {
  return function () {
    var deferred = new Deferred();
    Deferred.when(fn()).done(function () {
      var _arguments = arguments;

      var args = Array.prototype.slice.call(arguments);
      if (predicate && predicate.apply(null, args)) {
        API_FUNCTION.login.refresh().done(function () {
          deferred.resolve.apply(deferred, args);
        }).fail(function () {
          deferred.reject.apply(deferred, Array.prototype.slice.call(_arguments));
        });
      } else {
        deferred.resolve.apply(deferred, args);
      }
    }).fail(function () {
      deferred.reject.apply(deferred, Array.prototype.slice.call(arguments));
    });
    return deferred;
  };
}

module.exports = API_FUNCTION;

/***/ }),

/***/ 23:
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ }),

/***/ 24:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var extend = __webpack_require__(25);
var urlPath = __webpack_require__(5);
var stream = weex.requireModule('stream');
var Deferred = __webpack_require__(1);
var serialize = __webpack_require__(26);
var login = __webpack_require__(13);
var loginDoing = false;
var modal = weex.requireModule("modal");
var login_options = [];
var broadcastChannelKeys = __webpack_require__(12);
var loginBroadcastChannel = new BroadcastChannel(broadcastChannelKeys.LOGIN);
var navigator = __webpack_require__(4);
loginBroadcastChannel.onmessage = function (e) {
  loginDoing = false;
};
var API = {
  request: function request(option) {
    option = extend({}, option || {});
    option.headers = option.headers || {};
    if (!option.headers['Content-Type']) {
      option.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    }
    var deferred = new Deferred();
    deferred.fail(function (data, res) {
      if (res.errMsg) {
        modal.alert({
          title: '信息提示',
          message: res.errMsg.split(' ')[1],
          success: function success() {}
        });
      }
    });
    stream.fetch(option, function (res) {
      var _arguments = arguments;

      var data = res.data;
      if (res === undefined || data === undefined) {
        deferred.reject(data, res);
        return;
      }
      if (data.success) {
        deferred.resolve(res.data, res);
      } else {
        if (data.code === 403) {
          if (loginDoing) {
            login_options.push(option);
            return;
          }
          login_options.push(option);
          loginDoing = true;
          login.invalidate().done(function () {
            login.login(function () {
              loginDoing = false;
              while (login_options.length) {
                API.request(login_options.pop()).done(function () {
                  deferred.resolve.apply(deferred, Array.prototype.slice.call(arguments));
                }).fail(function (rs) {
                  deferred.reject.apply(deferred, Array.prototype.slice.call(_arguments));
                });
              }
            });
          });
        }
        if (data.code === 601) {
          if (data.data === 'SUPER') {
            modal.confirm({
              message: '对不起，您还不是超级会员，无法进行该操作！',
              duration: 0.3,
              okTitle: '升级超级会员',
              cancelTitle: '关闭'
            }, function (value) {
              if (value == '升级超级会员') {
                navigator.push('upgrade-user-level.js');
              }
            });
          }
        } else {
          if (data.level === 'SYSTEM') {
            modal.alert({
              title: '信息提示',
              message: data.errMsg,
              success: function success(res) {}
            });
            deferred.reject(res.data, res);
          }
        }
      }
    }, function () {
      deferred.notify.apply(deferred, Array.prototype.slice.call(arguments));
    });
    return deferred;
  },
  do_request: function do_request(url, param, method, type) {
    if (typeof url == 'function') {
      var tmp = url(param);
      if (typeof tmp == 'string') {
        url = tmp;
      } else {
        url = tmp[0];
        if (tmp.length > 1) {
          param = tmp[1];
        }
      }
    }
    url = urlPath.api(url);
    // 处理url中的placeholder
    url = url.replace(/\{([^}]+)}/gi, function ($0, $1) {
      var val = param[$1];
      delete param[$1];
      return val;
    });
    if (method.toLowerCase() === 'get') {
      if (url.indexOf('?') == -1) {
        url += '?';
      }
      url = url + (url.indexOf('&') == -1 ? '' : '&') + (typeof param !== 'string' ? serialize(param) : param === undefined ? '' : param);
      param = '';
    }
    // modal.alert({message: JSON.stringify(param)})
    return this.request({
      url: url,
      type: type,
      body: typeof param !== 'string' ? serialize(param) : param === undefined ? '' : param,
      method: method
    });
  },
  createFunction: function createFunction(url, method) {
    var self = this;
    return function (body, type, listener) {
      listener && listener.onRequest && listener.onRequest();
      return self.do_request(url, body, method || 'GET', type || 'json').done(function (rs) {
        listener && listener.onSuccess && listener.onSuccess(rs);
      }).fail(function (rs) {
        listener && listener.onFail && listener.onFail(rs);
      }).always(function (rs) {
        listener && listener.onFinish && listener.onFinish(rs);
      });
    };
  },
  createUrl: function createUrl(url) {
    url = urlPath.api(url);
    return function (param) {
      return url.replace(/\{([^}]+)}/gi, function ($0, $1) {
        var val = typeof param === 'string' ? param : param[$1];
        return val == undefined ? '' : val;
      });
    };
  }
};

module.exports = API;

/***/ }),

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var extend = function extend(target, source) {
	for (var key in source) {
		var val = source[key];
		target[key] = val;
	}
	return target;
};

module.exports = extend;

/***/ }),

/***/ 258:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Deferred = __webpack_require__(1);
var modal = weex.requireModule('modal');
module.exports = {
  data: function data() {
    return {
      refreshing: false,
      indicatorScale: 0.2,
      refreshPullingDown: false
    };
  },
  components: {
    'page-refresh': __webpack_require__(262)
  },
  methods: {
    onRefresh: function onRefresh(event, callback) {
      var _this = this;

      this.refreshing = true;
      Deferred.when(callback).always(function () {
        _this.refreshPullingDown = false;
        // this.indicatorScale = .2
        setTimeout(function () {
          _this.refreshing = false;
        }, 300);
      });
    },
    onPullingDown: function onPullingDown(event) {
      if (this.refreshing) {
        return;
      }
      this.refreshPullingDown = true;
      var scale = Math.abs(event.pullingDistance) / (event.viewHeight / 2) * 40 / 100;
      this.indicatorScale = scale > 1 ? 1 : scale;
    }
  }
};

/***/ }),

/***/ 259:
/***/ (function(module, exports) {

module.exports = {
  "refresh-container": {
    "width": 750,
    "alignItems": "center",
    "justifyContent": "center"
  },
  "refresh-indicator": {
    "width": 45,
    "height": 45,
    "borderStyle": "solid",
    "borderWidth": 6,
    "borderColor": "#fb4748",
    "transform": "scale(0.2)",
    "borderRadius": 50
  }
}

/***/ }),

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (obj) {
    var tmp = [];
    for (var key in obj) {
        var val = obj[key];
        if (Array.isArray(val)) {
            for (var i = 0; i < val.length; i++) {
                tmp.push(key + "[" + i + "]=" + val[i]);
            }
        } else {
            tmp.push(key + "=" + encodeURIComponent(val));
        }
    }
    return tmp.join("&");
};

/***/ }),

/***/ 260:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


module.exports = {
  props: {
    refreshing: { default: false },
    indicatorScale: { default: 0.2 }
  },

  data: function data() {
    return {
      refreshHeight: 100
    };
  },

  components: {},
  created: function created() {},

  methods: {
    onRefresh: function onRefresh(event) {
      this.$emit('refresh', event);
    },
    onPullingDown: function onPullingDown(event) {
      // this.refreshHeight = -event.pullingDistance
      this.$emit('pullingDown', event);
    }
  }
};

/***/ }),

/***/ 261:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('refresh', {
    staticClass: ["refresh"],
    style: {
      height: _vm.refreshHeight
    },
    attrs: {
      "display": _vm.refreshing ? 'show' : 'hide'
    },
    on: {
      "refresh": _vm.onRefresh,
      "pullingdown": _vm.onPullingDown
    }
  }, [_c('div', {
    staticClass: ["refresh-container"],
    style: {
      height: _vm.refreshHeight
    }
  }, [_c('div', {
    staticClass: ["refresh-indicator"],
    style: {
      transform: 'scale(' + _vm.indicatorScale + ')'
    }
  })])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 262:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(259)
)

/* script */
__vue_exports__ = __webpack_require__(260)

/* template */
var __vue_template__ = __webpack_require__(261)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/lvling/workspace/laxiaoke-app/src/components/page-refresh.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-25d9afff"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 264:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
  components: {
    'page-scrolltop': __webpack_require__(270)
  },
  data: function data() {
    return {
      pageScrollTopShown: false
    };
  },

  methods: {
    onPageScrollForScrollTop: function onPageScrollForScrollTop(event) {
      var y = Math.abs(event.contentOffset.y);
      if (y > this.$getConfig().env.deviceHeight * 2 / 3) {
        this.pageScrollTopShown = true;
      } else {
        this.pageScrollTopShown = false;
      }
    }
  }
};

/***/ }),

/***/ 265:
/***/ (function(module, exports) {

module.exports = {
  "icon": {
    "fontFamily": "icon"
  },
  "ui-to-top": {
    "position": "fixed",
    "right": "30",
    "width": "68",
    "height": "68",
    "justifyContent": "center",
    "alignItems": "center",
    "borderRadius": "68",
    "overflow": "hidden",
    "backgroundColor": "#ffffff",
    "borderStyle": "solid",
    "borderWidth": "1",
    "borderColor": "#fa1516",
    "color": "#ffffff",
    "boxShadow": "0px 0px 10px rgba(102, 102, 102, 0.62)",
    "zIndex": 1000
  },
  "ui-to-top-icon": {
    "fontSize": "36",
    "lineHeight": "36",
    "color": "#fb4748"
  }
}

/***/ }),

/***/ 266:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

__webpack_require__(9);
module.exports = {
  props: {
    shown: { default: false },
    bottom: { default: 120 }
  },

  mixins: [],

  data: function data() {
    return {};
  },

  components: {},
  created: function created() {},

  methods: {
    gotoTop: function gotoTop() {
      this.$emit('click');
    }
  }
};

/***/ }),

/***/ 267:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.shown) ? _c('div', {
    staticClass: ["ui-to-top"],
    style: {
      bottom: _vm.bottom
    },
    on: {
      "click": _vm.gotoTop
    }
  }, [_c('text', {
    staticClass: ["icon", "ui-to-top-icon"]
  }, [_vm._v("")])]) : _vm._e()
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 268:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
  data: function data() {
    return {
      loadingMore: false
    };
  },
  components: {
    'page-loading-more': __webpack_require__(279)
  },
  methods: {}
};

/***/ }),

/***/ 270:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(265)
)

/* script */
__vue_exports__ = __webpack_require__(266)

/* template */
var __vue_template__ = __webpack_require__(267)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/lvling/workspace/laxiaoke-app/src/components/page-scrolltop.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-3321870c"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 271:
/***/ (function(module, exports) {

module.exports = {
  "loading-more-gif": {
    "width": "80",
    "height": "80"
  },
  "loading-more-container": {
    "height": "80",
    "alignItems": "center",
    "justifyContent": "center"
  },
  "loading-more-tip": {
    "height": "160",
    "alignItems": "center",
    "justifyContent": "center",
    "flexDirection": "row"
  },
  "loading-more-tip-text": {
    "color": "#8c8c8c",
    "fontSize": 24
  },
  "loading-more-tip-line": {
    "height": "1",
    "backgroundColor": "#dddddd",
    "marginTop": 0,
    "marginRight": "20",
    "marginBottom": 0,
    "marginLeft": "20"
  }
}

/***/ }),

/***/ 272:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var urlPath = __webpack_require__(5);
module.exports = {
  props: {
    loadingMore: { default: false },
    width: { default: 0 },
    shown: { default: true }
  },

  data: function data() {
    return {
      loadMoreGifSrc: 'images/gif/loading.gif',
      panelWidth: this.width
    };
  },

  components: {},
  created: function created() {
    if (!this.panelWidth) {
      this.panelWidth = 750;
    }
    this.loadMoreGifSrc = urlPath.assets(this.loadMoreGifSrc, true);
  },

  methods: {
    onLoading: function onLoading(event) {
      this.$emit('loadingMore', event);
    }
  }
};

/***/ }),

/***/ 273:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.shown) ? _c('loading', {
    attrs: {
      "display": _vm.loadingMore ? 'show' : 'hide'
    },
    on: {
      "loading": _vm.onLoading
    }
  }, [_c('div', {
    staticClass: ["loading-more-container"],
    style: {
      width: _vm.panelWidth
    }
  }, [_c('image', {
    staticClass: ["loading-more-gif"],
    attrs: {
      "src": _vm.loadMoreGifSrc
    }
  })])]) : _c('cell', {
    staticClass: ["loading-more-tip"],
    style: {
      width: _vm.panelWidth
    },
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('div', {
    staticClass: ["loading-more-tip-line"],
    style: {
      width: _vm.panelWidth * 0.2
    }
  }), _c('text', {
    staticClass: ["loading-more-tip-text"]
  }, [_vm._v("我是有底线的")]), _c('div', {
    staticClass: ["loading-more-tip-line"],
    style: {
      width: _vm.panelWidth * 0.2
    }
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 274:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(275)
)

/* script */
__vue_exports__ = __webpack_require__(276)

/* template */
var __vue_template__ = __webpack_require__(277)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/lvling/workspace/laxiaoke-app/src/components/page-notice.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-0b88c838"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 275:
/***/ (function(module, exports) {

module.exports = {
  "ui-page-msg": {
    "flexDirection": "column",
    "alignItems": "center",
    "justifyContent": "center"
  },
  "ui-page-msg-text-desc": {
    "fontSize": "28",
    "color": "#8c8c8c"
  }
}

/***/ }),

/***/ 276:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var dom = weex.requireModule('dom');
var urlPath = __webpack_require__(5);
module.exports = {

  props: {
    icon: { default: urlPath.assets('images/aiya.png') },
    iconWidth: { default: 240 },
    iconHeight: { default: 240 },
    msg: { default: '温馨提示' },
    offset: { default: 0 }
  },

  data: function data() {
    return {
      width: 0,
      height: 0,
      marginBottom: 0,
      opacity: 0
    };
  },

  components: {},
  created: function created() {
    if (!this.width) {
      this.width = 750;
    }
  },
  mounted: function mounted() {
    var _this = this;

    setTimeout(function () {
      var deviceHeight = weex.config.env.deviceHeight;
      dom.getComponentRect(_this.$refs.container, function (rs) {
        if (rs.result) {
          _this.height = deviceHeight - rs.size.top * 2;
          _this.marginBottom = rs.size.top * 750 / weex.config.env.deviceWidth;
        } else {
          _this.height = deviceHeight;
        }
        _this.height = (_this.height + _this.offset) * 750 / weex.config.env.deviceWidth;
        _this.opacity = 1;
      });
    }, 100);
  },

  methods: {}
};

/***/ }),

/***/ 277:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    ref: "container",
    staticClass: ["ui-page-msg"],
    style: {
      width: _vm.width,
      height: _vm.height,
      marginBottom: _vm.marginBottom,
      opacity: _vm.opacity
    }
  }, [(_vm.icon) ? _c('div', {
    staticClass: ["ui-page-msg-icon"]
  }, [_c('image', {
    style: {
      width: _vm.iconWidth,
      height: _vm.iconHeight
    },
    attrs: {
      "src": _vm.icon
    }
  })]) : _vm._e(), _c('div', {
    staticClass: ["ui-page-msg-text"]
  }, [_c('text', {
    staticClass: ["ui-page-msg-text-desc"]
  }, [_vm._v(_vm._s(_vm.msg))])])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 279:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(271)
)

/* script */
__vue_exports__ = __webpack_require__(272)

/* template */
var __vue_template__ = __webpack_require__(273)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/lvling/workspace/laxiaoke-app/src/components/page-loading-more.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-d2254f5c"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 28:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(29)
)

/* script */
__vue_exports__ = __webpack_require__(30)

/* template */
var __vue_template__ = __webpack_require__(31)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/lvling/workspace/laxiaoke-app/src/components/navbar.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-dd9eff1c"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 29:
/***/ (function(module, exports) {

module.exports = {
  "icon": {
    "fontFamily": "icon"
  },
  "activeable": {
    "opacity": 1,
    "opacity:active": 0.6
  },
  "navbar": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "paddingTop": 40,
    "alignItems": "center",
    "position": "relative"
  },
  "navbar-gradient": {
    "backgroundImage": "linear-gradient(to right, #fb4748, #fb4748)"
  },
  "navbar-primary": {
    "backgroundColor": "#fb4748"
  },
  "navbar-light": {
    "backgroundColor": "#F9F9F9",
    "backgroundImage": "linear-gradient(to right, #F9F9F9, #F9F9F9)",
    "borderStyle": "solid",
    "borderWidth": 1,
    "borderColor": "#dddddd"
  },
  "navbar-fixed": {
    "position": "fixed",
    "top": 0,
    "right": 0,
    "left": 0
  },
  "icon-item": {
    "fontSize": 36,
    "width": 64,
    "height": 64,
    "lineHeight": 64,
    "textAlign": "center",
    "borderRadius": 64,
    "color": "#ffffff",
    "backgroundColor": "rgba(0,0,0,0)"
  },
  "icon-item-transparent": {
    "backgroundColor": "rgba(40,40,40,0.62)"
  },
  "icon-item-transparent-full": {
    "color": "#474747"
  },
  "icon-item-light": {
    "color": "#474747"
  },
  "title": {
    "color": "#ffffff",
    "fontSize": 36,
    "overflow": "hidden",
    "textOverflow": "ellipsis",
    "lines": 1
  },
  "title-light": {
    "color": "#474747"
  },
  "title-transparent-full": {
    "color": "#474747"
  },
  "left": {
    "flexDirection": "row",
    "alignItems": "center",
    "paddingLeft": 15
  },
  "left-text": {
    "fontSize": 36,
    "textAlign": "center",
    "color": "#ffffff"
  },
  "left-text-transparent": {
    "backgroundColor": "rgba(40,40,40,0.62)"
  },
  "left-text-transparent-full": {
    "color": "#474747"
  },
  "left-text-light": {
    "color": "#474747"
  },
  "right": {
    "alignItems": "flex-end",
    "justifyContent": "flex-end",
    "paddingRight": 15
  },
  "right-text": {
    "fontSize": 32,
    "color": "#ffffff",
    "textAlign": "center"
  },
  "right-text-transparent": {
    "backgroundColor": "rgba(40,40,40,0.62)"
  },
  "right-text-transparent-full": {
    "color": "#474747"
  },
  "right-text-light": {
    "color": "#474747"
  },
  "center": {
    "position": "absolute",
    "left": 0,
    "right": 0,
    "top": 40,
    "height": 90,
    "justifyContent": "center",
    "alignItems": "center"
  },
  "center-node": {
    "flex": 1,
    "paddingTop": 0,
    "paddingRight": 15,
    "paddingBottom": 0,
    "paddingLeft": 15
  }
}

/***/ }),

/***/ 30:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var navigator = __webpack_require__(4);
module.exports = {
  mixins: [__webpack_require__(9)],
  props: {
    title: { default: '' },
    theme: { default: 'transparent' },
    height: { default: 130 },
    fixed: { default: false },
    rightItemText: { default: '' },
    scrollOffsetY: { default: 0 },
    transformTheme: { default: 'light' },
    showTitle: { default: true },
    rightNode: { default: false },
    rightItemIcon: { default: '' },
    leftItemIcon: { default: '' },
    leftItemText: { default: '' },
    leftBack: { default: true },
    titleNode: { default: false },
    leftShow: { default: true },
    rightShow: { default: true }
  },

  data: function data() {
    return {
      navbarOpacity: 1,
      _theme: ''
    };
  },

  components: {},
  created: function created() {
    var _this = this;

    this._theme = this.theme;
    this.$watch('scrollOffsetY', function () {
      _this.handleScrollTop();
    });
    this.$watch('theme', function (newVal, oldVal) {
      _this._theme = newVal;
    });
  },

  methods: {
    rightItemClick: function rightItemClick() {
      this.$emit('rightItemClick');
    },
    leftItemClick: function leftItemClick(e) {
      if (this.leftBack) {
        navigator.pop();
      } else {
        this.$emit('leftItemClick', e);
      }
    },
    handleScrollTop: function handleScrollTop() {
      if (this.scrollOffsetY >= 0) {
        if (this._theme !== this.theme) {
          this.navbarOpacity = 1;
          this._theme = this.theme;
        }
      } else {
        var offsetY = -300;
        var opacity = this.scrollOffsetY / offsetY;
        if (opacity <= 1) {
          // modal.toast({message: 2, duration: 0.001})
          this.navbarOpacity = opacity;
        } else if (this.navbarOpacity != 1) {
          this.navbarOpacity = 1;
        }
        if (this._theme !== this.transformTheme) {
          this.navbarOpacity = 0;
          this._theme = this.transformTheme;
        }
      }
    }
  }
};

/***/ }),

/***/ 31:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    class: ['navbar', 'navbar-' + _vm._theme, _vm.fixed && 'navbar-fixed'],
    style: {
      height: _vm.height,
      opacity: _vm.navbarOpacity
    }
  }, [((_vm.showTitle && _vm.title)) ? _c('div', {
    staticClass: ["center"]
  }, [_c('text', {
    class: ['title', 'title-' + _vm._theme]
  }, [_vm._v(_vm._s(_vm.title))])]) : _vm._e(), (_vm.leftShow) ? _c('div', {
    staticClass: ["left"]
  }, [_c('text', {
    class: ['activeable', 'icon', 'icon-item', 'icon-item-' + _vm._theme, 'back'],
    on: {
      "click": _vm.leftItemClick
    }
  }, [_vm._v(_vm._s(_vm.leftItemIcon || '\ue600'))]), (_vm.leftItemText) ? _c('text', {
    class: ['left-text', 'left-text-' + _vm._theme],
    on: {
      "click": _vm.leftItemClick
    }
  }, [_vm._v(_vm._s(_vm.leftItemText))]) : _vm._e()]) : _vm._e(), (_vm.titleNode) ? _c('div', {
    staticClass: ["center-node"]
  }, [(_vm.showTitle && _vm.titleNode) ? _vm._t("titleNode") : _vm._e()], 2) : _vm._e(), (_vm.rightShow) ? _c('div', {
    staticClass: ["right"]
  }, [(_vm.rightNode) ? [_vm._t("rightNode")] : _vm._e(), (!_vm.right) ? [(_vm.rightItemText || _vm.rightItemIcon) ? _c('text', {
    class: ['activeable', 'icon', 'right-text', 'right-text-' + _vm._theme, _vm.rightItemIcon && 'icon-item', _vm.rightItemIcon && 'icon-item-' + _vm._theme],
    on: {
      "click": _vm.rightItemClick
    }
  }, [_vm._t("rightItemIcon"), _vm._v(_vm._s(_vm.rightItemText))], 2) : _vm._e()] : _vm._e()], 2) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var navigator = weex.requireModule('navigator');
var urlPath = __webpack_require__(5);
var Deferred = __webpack_require__(1);
var modal = weex.requireModule('modal');
module.exports = {
  push: function push(options, callback) {
    if (typeof options === 'string') {
      options = {
        url: options
      };
    }
    var deferred = new Deferred();
    urlPath.page(options.url, function (localJs) {
      options.url = localJs;
      navigator.push(options, function (rs) {
        callback && callback(rs);
        if (rs !== 'WX_SUCCESS') {
          modal.alert({ message: JSON.stringify(options) });
        }
        deferred.resolve(rs);
      });
    });
    return deferred.promise();
  },
  pop: function pop(options, callback) {
    var deferred = new Deferred();
    navigator.pop(options || {}, function (rs) {
      callback && callback(rs);
      deferred.resolve(rs);
    });
    return deferred.promise();
  }
};

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bundleJsService = weex.requireModule('bundleJsService');
var bundleJsServer = "http://bundlejs.laxiaoke.com";
var storageKeys = __webpack_require__(8);
var storage = __webpack_require__(7);

if (typeof weex.config.env.debug == 'string') {
  weex.config.env.debug = weex.config.env.debug === 'true';
}

var DEBUG = weex.config.env.debug;

var mode = 'my';
var assetsBase, pageBase, apiBase;
function setEnv(env) {
  switch (env) {
    case 'pro':
      assetsBase = 'http://imgs.laxiaoke.com/assets';
      pageBase = 'https://bundlejs.laxiaoke.com/';
      bundleJsServer = 'https://bundlejs.laxiaoke.com';
      apiBase = 'https://api.laxiaoke.com';
      break;
    case 'my':
      assetsBase = 'http://192.168.31.174/lxk-assets';
      pageBase = 'http://192.168.31.174/lxk';
      apiBase = 'http://192.168.31.174:8083/';
      DEBUG = true;
      break;
  }
}
setEnv(mode);

// config.config().done(cfg => {
//   if((cfg.debug && cfg.devEnv) || weex.config.env.debug) {
//     mode = 'dev'
//     setEnv(mode)
//   }
// })

function wrapVersion(path, flag) {
  if (!flag) {
    return path;
  }
  if (path.indexOf('?') != -1) {
    path += '&_=' + new Date().getTime();
  } else {
    path += '?_=' + new Date().getTime();
  }
  return path;
}

if (!pageBase.endsWith('/')) {
  pageBase += '/';
}
module.exports = {
  assets: function assets(path, version) {
    var base = assetsBase;
    return wrapVersion(base + '/' + path, version);
  },
  page: function page(path, version, callback) {
    if (typeof version == 'function') {
      callback = version;
      version = null;
    }
    var base = pageBase;
    if (path.startsWith('//')) {
      base = 'http:';
    } else if (path.startsWith('http://') || path.startsWith('https://')) {
      base = '';
    } else if (!DEBUG) {
      // } else {
      storage.getItem(storageKeys.BUNDLE_JS_VERSION).done(function (version) {
        var server = DEBUG ? pageBase : bundleJsServer + '/' + version + '/';
        var pathSeg = path.split('?');
        var basePath = pathSeg[0];
        bundleJsService.local({
          remote: server + basePath + '?_=' + new Date().getTime(),
          local: basePath
        }, function (localJs) {
          callback && callback(wrapVersion(localJs + (pathSeg.length > 1 ? '?' + pathSeg[1] : ''), version));
        });
      });
      return;
    }
    callback && callback(wrapVersion(base + path, version));
  },
  api: function api(path, version) {
    var base = apiBase;
    return wrapVersion(base + '/' + path, version);
  }
};

/***/ }),

/***/ 662:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(663)
)

/* script */
__vue_exports__ = __webpack_require__(664)

/* template */
var __vue_template__ = __webpack_require__(665)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/lvling/workspace/laxiaoke-app/src/template.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-5c6351c1"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 663:
/***/ (function(module, exports) {

module.exports = {
  "f-sm": {
    "fontSize": 24
  },
  "f": {
    "fontSize": 28
  },
  "flex-row": {
    "flexDirection": "row",
    "alignItems": "center"
  },
  "align-flex-end": {
    "alignItems": "flex-end"
  },
  "text-gold": {
    "color": "#ffe200"
  },
  "text-danger": {
    "color": "#d9534f"
  },
  "text-warning": {
    "color": "#e39334"
  },
  "text-purple": {
    "color": "#CA63EE"
  },
  "text-pink": {
    "color": "#FC7DAE"
  },
  "text-orange": {
    "color": "#FD8C74"
  },
  "text-sky": {
    "color": "#71ABF8"
  },
  "text-primary": {
    "color": "#fb4748"
  },
  "text-info": {
    "color": "#409FD6"
  },
  "text-success": {
    "color": "#5cb85c"
  },
  "text-gray": {
    "color": "#555555"
  },
  "text-muted": {
    "color": "#8c8c8c"
  },
  "text-light": {
    "color": "#ababab"
  },
  "space-between": {
    "justifyContent": "space-between"
  },
  "align-end": {
    "alignItems": "flex-end"
  },
  "bg-white": {
    "backgroundColor": "#FFFFFF"
  },
  "bg-primary": {
    "backgroundColor": "#fb4748"
  },
  "bg-warning": {
    "backgroundColor": "#FD9F28"
  },
  "bd-l": {
    "borderLeftStyle": "solid",
    "borderLeftWidth": 1,
    "borderLeftColor": "#dddddd"
  },
  "bd-t": {
    "borderTopStyle": "solid",
    "borderTopWidth": 1,
    "borderTopColor": "#dddddd"
  },
  "bd-r": {
    "borderRightStyle": "solid",
    "borderRightWidth": 1,
    "borderRightColor": "#dddddd"
  },
  "bd-b": {
    "borderBottomStyle": "solid",
    "borderBottomWidth": 1,
    "borderBottomColor": "#dddddd"
  },
  "flex-1": {
    "flex": 1
  },
  "text-left": {
    "textAlign": "left"
  },
  "text-right": {
    "textAlign": "right"
  },
  "text-center": {
    "textAlign": "center"
  },
  "icon": {
    "fontFamily": "icon"
  },
  "container": {
    "backgroundColor": "#F5F5F5",
    "transitionProperty": "transform",
    "transitionDuration": 300,
    "transitionTimingFunction": "linear"
  },
  "@TRANSITION": {
    "container": {
      "property": "transform",
      "duration": 300,
      "timingFunction": "linear"
    }
  }
}

/***/ }),

/***/ 664:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var api = __webpack_require__(14);
var Deferred = __webpack_require__(1);
var dom = weex.requireModule('dom');
var urlPath = __webpack_require__(5);
var modal = weex.requireModule('modal');
var navigator = __webpack_require__(4);
__webpack_require__(9);
module.exports = {
  mixins: [__webpack_require__(258), __webpack_require__(268), __webpack_require__(264)],
  components: {
    'page-notice': __webpack_require__(274),
    navbar: __webpack_require__(28)
  },
  data: function data() {
    return {};
  },
  created: function created() {
    Deferred.when(this.loadData()).always(function () {});
  },

  methods: {
    loadData: function loadData() {},
    onXRefresh: function onXRefresh(event) {
      this.searchParam.page = 1;
      this.onRefresh(event, Deferred.when(this.loadData(), this.search()));
    },
    onLoadingMore: function onLoadingMore(event) {
      var _this = this;

      this.loadingMore = true;
      this.searchParam.page = this.searchParam.page + 1;
      this.searchItem(true, function () {
        _this.loadingMore = false;
      });
    },
    gotoTop: function gotoTop() {
      var dom = weex.requireModule('dom');
      dom.scrollToElement(this.$refs.commissionOrderList);
    },
    onPageScroll: function onPageScroll(event) {
      this.onPageScrollForScrollTop(event);
    },
    itemClick: function itemClick(item) {
      // navigator.push('commission-item.js?id=' + item.id)
    }
  }
};

/***/ }),

/***/ 665:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["container"]
  }, [_c('navbar', {
    attrs: {
      "title": "订单明细",
      "theme": "light"
    }
  }), _c('div', {
    staticClass: ["filterbar"]
  }), _c('list', {
    staticClass: ["order-list-container"],
    on: {
      "scroll": _vm.onPageScroll
    }
  }, [_c('page-refresh', {
    attrs: {
      "refreshing": _vm.refreshing,
      "indicatorScale": _vm.indicatorScale
    },
    on: {
      "refresh": _vm.onXRefresh,
      "pullingDown": _vm.onPullingDown
    }
  }), _c('cell', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [(!_vm.commissionOrderList.length) ? _c('page-notice', {
    attrs: {
      "msg": "抱歉，什么都木有找到～"
    }
  }) : _vm._e()], 1), _c('page-loading-more', {
    attrs: {
      "loadingMore": _vm.loadingMore,
      "shown": _vm.hasNext
    },
    on: {
      "loadingMore": _vm.onLoadingMore
    }
  })], 1), _c('page-scrolltop', {
    attrs: {
      "shown": _vm.pageScrollTopShown
    },
    on: {
      "click": _vm.gotoTop
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var storage = weex.requireModule('storage');
var Deferred = __webpack_require__(1);
module.exports = {
  setItem: function setItem(key, value) {
    var deferred = new Deferred();
    value = JSON.stringify(value);
    storage.setItem(key, value, function (rs) {
      deferred.resolve(rs);
    });
    return deferred.promise();
  },
  getItem: function getItem(key, defaultVal) {
    var deferred = new Deferred();
    storage.getItem(key, function (rs) {
      if (rs.result === "failed") {
        deferred.resolve(null || defaultVal, rs);
      } else {
        try {
          deferred.resolve(JSON.parse(rs.data), rs);
        } catch (e) {
          deferred.resolve(null || defaultVal, rs);
        }
      }
    });
    return deferred.promise();
  },
  removeItem: function removeItem(key) {
    var deferred = new Deferred();
    storage.removeItem(key, function (rs) {
      deferred.resolve(rs);
    });
    return deferred.promise();
  },
  length: function length() {
    var deferred = new Deferred();
    storage.length(function (rs) {
      deferred.resolve(rs.data, rs);
    });
    return deferred.promise();
  },
  getAllKeys: function getAllKeys(callback) {
    var deferred = new Deferred();
    storage.length(function (rs) {
      deferred.resolve(rs.data, rs);
    });
    return deferred.promise();
  }
};

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
  LOGIN: "LOGIN",
  BUNDLE_JS_VERSION: 'BUNDLE_JS_VERSION',
  USER_ITEM_SELECTION: 'USER_ITEM_SELECTION',
  CONFIG: 'CONFIG',
  NAVIGATOR_COUNT: 'NAVIGATOR_COUNT',
  DEBUG: 'DEBUG',
  PHONE_BIND: 'PHONE_BIND'
};

/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var domModule = weex.requireModule('dom');
domModule.addRule('fontFace', {
  'fontFamily': "icon",
  'src': "url('https://at.alicdn.com/t/font_565950_qoez9qmpwgo.ttf')"
});
module.exports = {};

/***/ })

/******/ });