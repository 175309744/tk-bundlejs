// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 414);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function (define) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {

        var forEach = Array.prototype.forEach;
        var hasOwn = Object.prototype.hasOwnProperty;
        var breaker = {};
        var isArray = function isArray(elem) {
            return (typeof elem === "undefined" ? "undefined" : _typeof(elem)) === "object" && elem instanceof Array;
        };
        var isFunction = function isFunction(fn) {
            return typeof fn === "function";
        };

        var // Static reference to slice
        sliceDeferred = [].slice;

        // String to Object flags format cache
        var flagsCache = {};

        // Convert String-formatted flags into Object-formatted ones and store in cache
        function createFlags(flags) {
            var object = flagsCache[flags] = {},
                i,
                length;
            flags = flags.split(/\s+/);
            for (i = 0, length = flags.length; i < length; i++) {
                object[flags[i]] = true;
            }
            return object;
        }

        // Borrowed shamelessly from https://github.com/wookiehangover/underscore.Deferred
        var _each = function _each(obj, iterator, context) {
            var key, i, l;

            if (!obj) {
                return;
            }
            if (forEach && obj.forEach === forEach) {
                obj.forEach(iterator, context);
            } else if (obj.length === +obj.length) {
                for (i = 0, l = obj.length; i < l; i++) {
                    if (i in obj && iterator.call(context, obj[i], i, obj) === breaker) {
                        return;
                    }
                }
            } else {
                for (key in obj) {
                    if (hasOwn.call(obj, key)) {
                        if (iterator.call(context, obj[key], key, obj) === breaker) {
                            return;
                        }
                    }
                }
            }
        };

        var Callbacks = function Callbacks(flags) {

            // Convert flags from String-formatted to Object-formatted
            // (we check in cache first)
            flags = flags ? flagsCache[flags] || createFlags(flags) : {};

            var // Actual callback list
            list = [],

            // Stack of fire calls for repeatable lists
            stack = [],

            // Last fire value (for non-forgettable lists)
            memory,

            // Flag to know if list is currently firing
            firing,

            // First callback to fire (used internally by add and fireWith)
            firingStart,

            // End of the loop when firing
            firingLength,

            // Index of currently firing callback (modified by remove if needed)
            firingIndex,

            // Add one or several callbacks to the list
            _add = function _add(args) {
                var i, length, elem, type, actual;
                for (i = 0, length = args.length; i < length; i++) {
                    elem = args[i];
                    if (isArray(elem)) {
                        // Inspect recursively
                        _add(elem);
                    } else if (isFunction(elem)) {
                        // Add if not in unique mode and callback is not in
                        if (!flags.unique || !self.has(elem)) {
                            list.push(elem);
                        }
                    }
                }
            },

            // Fire callbacks
            fire = function fire(context, args) {
                args = args || [];
                memory = !flags.memory || [context, args];
                firing = true;
                firingIndex = firingStart || 0;
                firingStart = 0;
                firingLength = list.length;
                for (; list && firingIndex < firingLength; firingIndex++) {
                    if (list[firingIndex].apply(context, args) === false && flags.stopOnFalse) {
                        memory = true; // Mark as halted
                        break;
                    }
                }
                firing = false;
                if (list) {
                    if (!flags.once) {
                        if (stack && stack.length) {
                            memory = stack.shift();
                            self.fireWith(memory[0], memory[1]);
                        }
                    } else if (memory === true) {
                        self.disable();
                    } else {
                        list = [];
                    }
                }
            },

            // Actual Callbacks object
            self = {
                // Add a callback or a collection of callbacks to the list
                add: function add() {
                    if (list) {
                        var length = list.length;
                        _add(arguments);
                        // Do we need to add the callbacks to the
                        // current firing batch?
                        if (firing) {
                            firingLength = list.length;
                            // With memory, if we're not firing then
                            // we should call right away, unless previous
                            // firing was halted (stopOnFalse)
                        } else if (memory && memory !== true) {
                            firingStart = length;
                            fire(memory[0], memory[1]);
                        }
                    }
                    return this;
                },
                // Remove a callback from the list
                remove: function remove() {
                    if (list) {
                        var args = arguments,
                            argIndex = 0,
                            argLength = args.length;
                        for (; argIndex < argLength; argIndex++) {
                            for (var i = 0; i < list.length; i++) {
                                if (args[argIndex] === list[i]) {
                                    // Handle firingIndex and firingLength
                                    if (firing) {
                                        if (i <= firingLength) {
                                            firingLength--;
                                            if (i <= firingIndex) {
                                                firingIndex--;
                                            }
                                        }
                                    }
                                    // Remove the element
                                    list.splice(i--, 1);
                                    // If we have some unicity property then
                                    // we only need to do this once
                                    if (flags.unique) {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    return this;
                },
                // Control if a given callback is in the list
                has: function has(fn) {
                    if (list) {
                        var i = 0,
                            length = list.length;
                        for (; i < length; i++) {
                            if (fn === list[i]) {
                                return true;
                            }
                        }
                    }
                    return false;
                },
                // Remove all callbacks from the list
                empty: function empty() {
                    list = [];
                    return this;
                },
                // Have the list do nothing anymore
                disable: function disable() {
                    list = stack = memory = undefined;
                    return this;
                },
                // Is it disabled?
                disabled: function disabled() {
                    return !list;
                },
                // Lock the list in its current state
                lock: function lock() {
                    stack = undefined;
                    if (!memory || memory === true) {
                        self.disable();
                    }
                    return this;
                },
                // Is it locked?
                locked: function locked() {
                    return !stack;
                },
                // Call all callbacks with the given context and arguments
                fireWith: function fireWith(context, args) {
                    if (stack) {
                        if (firing) {
                            if (!flags.once) {
                                stack.push([context, args]);
                            }
                        } else if (!(flags.once && memory)) {
                            fire(context, args);
                        }
                    }
                    return this;
                },
                // Call all the callbacks with the given arguments
                fire: function fire() {
                    self.fireWith(this, arguments);
                    return this;
                },
                // To know if the callbacks have already been called at least once
                fired: function fired() {
                    return !!memory;
                }
            };

            return self;
        };

        var Deferred = function Deferred(func) {
            var doneList = Callbacks("once memory"),
                failList = Callbacks("once memory"),
                progressList = Callbacks("memory"),
                _state = "pending",
                lists = {
                resolve: doneList,
                reject: failList,
                notify: progressList
            },
                _promise = {
                done: doneList.add,
                fail: failList.add,
                progress: progressList.add,

                state: function state() {
                    return _state;
                },

                // Deprecated
                isResolved: doneList.fired,
                isRejected: failList.fired,

                then: function then(doneCallbacks, failCallbacks, progressCallbacks) {
                    deferred.done(doneCallbacks).fail(failCallbacks).progress(progressCallbacks);
                    return this;
                },
                always: function always() {
                    deferred.done.apply(deferred, arguments).fail.apply(deferred, arguments);
                    return this;
                },
                pipe: function pipe(fnDone, fnFail, fnProgress) {
                    return Deferred(function (newDefer) {
                        _each({
                            done: [fnDone, "resolve"],
                            fail: [fnFail, "reject"],
                            progress: [fnProgress, "notify"]
                        }, function (data, handler) {
                            var fn = data[0],
                                action = data[1],
                                returned;
                            if (isFunction(fn)) {
                                deferred[handler](function () {
                                    returned = fn.apply(this, arguments);
                                    if (returned && isFunction(returned.promise)) {
                                        returned.promise().then(newDefer.resolve, newDefer.reject, newDefer.notify);
                                    } else {
                                        newDefer[action + "With"](this === deferred ? newDefer : this, [returned]);
                                    }
                                });
                            } else {
                                deferred[handler](newDefer[action]);
                            }
                        });
                    }).promise();
                },
                // Get a promise for this deferred
                // If obj is provided, the promise aspect is added to the object
                promise: function promise(obj) {
                    if (!obj) {
                        obj = _promise;
                    } else {
                        for (var key in _promise) {
                            obj[key] = _promise[key];
                        }
                    }
                    return obj;
                }
            },
                deferred = _promise.promise({}),
                key;

            for (key in lists) {
                deferred[key] = lists[key].fire;
                deferred[key + "With"] = lists[key].fireWith;
            }

            // Handle state
            deferred.done(function () {
                _state = "resolved";
            }, failList.disable, progressList.lock).fail(function () {
                _state = "rejected";
            }, doneList.disable, progressList.lock);

            // Call given func if any
            if (func) {
                func.call(deferred, deferred);
            }

            // All done!
            return deferred;
        };

        // Deferred helper
        var when = function when(firstParam) {
            var args = sliceDeferred.call(arguments, 0),
                i = 0,
                length = args.length,
                pValues = new Array(length),
                count = length,
                pCount = length,
                deferred = length <= 1 && firstParam && isFunction(firstParam.promise) ? firstParam : Deferred(),
                promise = deferred.promise();
            function resolveFunc(i) {
                return function (value) {
                    args[i] = arguments.length > 1 ? sliceDeferred.call(arguments, 0) : value;
                    if (! --count) {
                        deferred.resolveWith(deferred, args);
                    }
                };
            }
            function progressFunc(i) {
                return function (value) {
                    pValues[i] = arguments.length > 1 ? sliceDeferred.call(arguments, 0) : value;
                    deferred.notifyWith(promise, pValues);
                };
            }
            if (length > 1) {
                for (; i < length; i++) {
                    if (args[i] && args[i].promise && isFunction(args[i].promise)) {
                        args[i].promise().then(resolveFunc(i), deferred.reject, progressFunc(i));
                    } else {
                        --count;
                    }
                }
                if (!count) {
                    deferred.resolveWith(deferred, args);
                }
            } else if (deferred !== firstParam) {
                deferred.resolveWith(deferred, length ? [firstParam] : []);
            }
            return promise;
        };

        Deferred.when = when;
        Deferred.Callbacks = Callbacks;

        return Deferred;
    }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)); // define for AMD if available
})(__webpack_require__(9));

/***/ }),

/***/ 414:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(415)
)

/* script */
__vue_exports__ = __webpack_require__(416)

/* template */
var __vue_template__ = __webpack_require__(417)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/lvling/workspace/laxiaoke-app/src/test.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-51c3a54e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 415:
/***/ (function(module, exports) {

throw new Error("Module build failed: \n  <scroller style='align-items:center; margin-top: 200'>\n^\n      File to import not found or unreadable: /Users/lvling/workspace/laxiaoke-app/src/sass/utilities.scss.\n      in /Users/lvling/workspace/laxiaoke-app/src/test.vue (line 2, column 1)");

/***/ }),

/***/ 416:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var navigator = __webpack_require__(428);
module.exports = {
  mixins: [__webpack_require__(436)],
  beforeCreate: function beforeCreate() {

    var domModule = weex.requireModule('dom');
    //目前支持ttf、woff文件，不支持svg、eot类型,moreItem at http://www.iconfont.cn/

    domModule.addRule('fontFace', {
      'fontFamily': "iconfont2",
      'src': "url('http://at.alicdn.com/t/font_1469606063_76593.ttf')"
    });
    domModule.addRule('fontFace', {
      'fontFamily': "iconfont3",
      'src': "url('http://at.alicdn.com/t/font_1469606522_9417143.woff')"
    });

    domModule.addRule('fontFace', {
      'fontFamily': "iconfont4",
      'src': "url('http://at.alicdn.com/t/font_zn5b3jswpofuhaor.ttf')"
    });

    domModule.addRule('fontFace', {
      'fontFamily': "iconfont5",
      'src': "url('https://at.alicdn.com/t/font_565950_222c9332lyhc9pb9.ttf?t=1520593417220')"
    });

    // you can use the absolute path or the local:// scheme path
    //  domModule.addRule('fontFace', {
    //   'fontFamily': "iconfont4",
    //   'src': "url('file:///storage/emulated/0/Android/data/com.alibaba.weex/cache/http:__at.alicdn.com_t_font_1469606063_76593.ttf')"
    // });
  },
  methods: {
    login: function login() {
      var modal = weex.requireModule('modal');
      modal.alert({ message: JSON.stringify(Nat) });
      Nat.image.preview('http://cdn.instapp.io/nat/samples/01.jpeg');
    },
    my: function my() {
      navigator.push('my.js');
    }
  }
};

/***/ }),

/***/ 417:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('scroller', {
    staticStyle: {
      alignItems: "center",
      marginTop: "200"
    }
  }, [_c('text', {
    on: {
      "click": _vm.login
    }
  }, [_vm._v("登录")]), _c('text', {
    on: {
      "click": _vm.my
    }
  }, [_vm._v("个人")]), _vm._m(0), _c('text', {
    staticClass: ["icon"]
  }, [_vm._v("")]), _c('text', {
    staticClass: ["title2"],
    staticStyle: {
      marginTop: "50px",
      width: "500px"
    }
  }, [_vm._v("http ttf: ")]), _c('text', {
    staticClass: ["title3"],
    staticStyle: {
      marginTop: "50px",
      width: "500px"
    }
  }, [_vm._v("http woff: ")]), _vm._m(1), _vm._m(2), _vm._m(3), _vm._m(4), _vm._m(5)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      marginTop: "20px"
    }
  }, [_c('text', {
    staticStyle: {
      color: "red",
      fontSize: "50px"
    }
  }, [_vm._v("only support font for ttf and woff")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      flexDirection: "row",
      marginTop: "50px"
    }
  }, [_c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "50",
      color: "green"
    }
  }, [_vm._v("")]), _c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "50"
    }
  }, [_vm._v("")]), _c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "60",
      color: "blue"
    }
  }, [_vm._v("")]), _c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "60",
      color: "green"
    }
  }, [_vm._v("")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      flexDirection: "row",
      marginTop: "50px"
    }
  }, [_c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "50",
      color: "green"
    }
  }, [_vm._v("")]), _c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "50"
    }
  }, [_vm._v("")]), _c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "60",
      color: "blue"
    }
  }, [_vm._v("")]), _c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "60",
      color: "green"
    }
  }, [_vm._v("")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "100",
      marginTop: "50px"
    }
  }, [_vm._v("")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "100",
      color: "green",
      marginTop: "50px"
    }
  }, [_vm._v("")]), _c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "100",
      marginTop: "50px"
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      flexDirection: "row"
    }
  }, [_c('text', {
    staticStyle: {
      fontFamily: "iconfont4",
      fontSize: "70",
      width: "750px"
    }
  }, [_vm._v("")])])
}]}
module.exports.render._withStripped = true

/***/ }),

/***/ 426:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function wrapVersion(path, flag) {
  if (!flag) {
    return path;
  }
  if (path.indexOf('?') != -1) {
    path += '&_=' + new Date().getTime();
  } else {
    path += '?_=' + new Date().getTime();
  }
  return path;
}

var mode = 'my';
var assetsBase, pageBase, apiBase;
switch (mode) {
  case 'pro':
    assetsBase = 'https://app.laxiaoke.com/assets/';
    switch (weex.env.platform.toLowerCase()) {
      case 'android':
        pageBase = '';
        break;
      case 'ios':
        var bundleUrl = weex.config.bundleUrl;
        pageBase = bundleUrl.substring(0, bundleUrl.indexOf('bundlejs') + 8);
        break;
      default:
        pageBase = 'https://bundlejs.laxiaoke.com';
        break;
    }
    apiBase = 'https://api.laxiaoke.com';
    break;
  case 'dev':
    assetsBase = 'http://dev.accountbook.api.batcloud.info:9090/api';
    pageBase = 'http://dev.accountbook.api.batcloud.info:9090/api';
    apiBase = 'http://dev.accountbook.api.batcloud.info:9090/api';
    break;
  case 'my':
    assetsBase = 'http://192.168.31.174/lxk-assets';
    pageBase = 'http://192.168.31.174/lxk';
    apiBase = 'http://192.168.31.174:8083/';
    break;
  case '606':
    assetsBase = 'http://192.168.18.119/lxk-assets';
    pageBase = 'http://192.168.18.119/lxk';
    apiBase = 'http://192.168.18.119:8083/';
    break;
  case 'zz':
    assetsBase = 'http://192.168.1.118:8083/';
    pageBase = 'http://192.168.1.118:8083/';
    apiBase = 'http://192.168.1.118:8083/';
    break;
}
if (!pageBase.endsWith('/')) {
  pageBase += '/';
}
module.exports = {
  assets: function assets(path, version) {
    var base = assetsBase;
    if (!weex.config.env.debug) {
      base = 'http://imgs.laxiaoke.com/assets';
    }
    return wrapVersion(base + '/' + path, version);
  },
  page: function page(path, version) {
    var base = pageBase;
    if (path.startsWith('//')) {
      base = 'http:';
    } else if (path.startsWith('http://') || path.startsWith('https://')) {
      base = '';
    } else {
      if (!weex.config.env.debug) {
        base = weex.config.env.bundleJsLocalDir;
      }
    }
    return wrapVersion(base + path, version);
  },
  api: function api(path, version) {
    var base = apiBase;
    if (!weex.config.env.debug) {
      base = 'https://api.laxiaoke.com';
    }
    return wrapVersion(base + '/' + path, version);
  }
};

/***/ }),

/***/ 428:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var navigator = weex.requireModule('navigator');
var urlPath = __webpack_require__(426);
var Deferred = __webpack_require__(1);
module.exports = {
  push: function push(options, callback) {
    if (typeof options === 'string') {
      options = {
        url: options
      };
    }
    options.url = urlPath.page(options.url);
    var deferred = new Deferred();
    navigator.push(options, function (rs) {
      callback && callback(rs);
      deferred.resolve(rs);
    });
    return deferred.promise();
  },
  pop: function pop(options, callback) {
    var deferred = new Deferred();
    navigator.pop(options || {}, function (rs) {
      callback && callback(rs);
      deferred.resolve(rs);
    });
    return deferred.promise();
  }
};

/***/ }),

/***/ 436:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var domModule = weex.requireModule('dom');
domModule.addRule('fontFace', {
  'fontFamily': "icon",
  'src': "url('https://at.alicdn.com/t/font_565950_k7zv3vncxflxr.ttf')"
});
module.exports = {};

/***/ }),

/***/ 9:
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ })

/******/ });